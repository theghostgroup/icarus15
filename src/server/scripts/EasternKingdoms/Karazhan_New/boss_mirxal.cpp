/*
Icarus Gaming Custom Boss
© Copyright Icarus Gaming Inc 2013. All rights reserved.
By Josh Carter
*/

/*
ScriptData
SDCategory: Karazhan_New
SDName: Boss Mirxal'Algothal -> The Soul Thief
SD%Complete: 95
SDComment: Needs Tested In Game
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

/* for multiple id's me->SetDisplayId(me->GetCreatureInfo()->Modelid1); and me->SetDisplayId(me->GetCreatureInfo()->Modelid2); */
enum MiraxalSpawnIds
{
	NPC_MIRXAL_BOSS				= 300500, // Change this after npc creation.
};

enum MiraxalSpells
{
	SPELL_BERSERK               = 41924,
	SPELL_SUNDER_ARMOR			= 59350, 
	SPELL_DRAGON_BREATH			= 29964, // On Fire and Confused (for lowest on threatlist)
	SPELL_DRAGON_CLAW 			= 74524,
	SPELL_SOULFIRE				= 6353,
	SPELL_DRAGON_ENGULF			= 74562,
	SPELL_DRAGON_BREATH_PTWO	= 74525,
	SPELL_DRAGON_BERSERK 		= 26662,
	SPELL_DRAGON_MASSIVE_FLAMESTRIKE = 26558,
	SPELL_DRAGON_FIREBALL_BARRAGE = 37541,
	SPELL_CHARRED_EARTH			= 30129,
	SPELL_DRAGON_FIRE_SHIELD 	= 57108, //Reduces damage by 95% for 5s
	SPELL_DEATH_AND_DECAY		= 72110,
	
};

enum MiraxalYells
{
	SOUND_ONAGGRO               = 0,
	SOUND_ONSLAY                = 1,
	SOUND_ONDEATH               = 2,
};

class boss_mirxal : public CreatureScript
{
public:
	boss_mirxal() : CreatureScript("boss_mirxal") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_mirxalAI (creature);
	}

	struct boss_mirxalAI : public ScriptedAI
	{
		boss_mirxalAI(Creature* pCreature) : ScriptedAI(pCreature) {
			x = me->GetPositionX();
			y = me->GetPositionY();
			z = me->GetPositionZ();
		}
		float x,y,z;
		// Enraged Spells
		uint32 FingerOfDeath_Timer;
		uint32 EnrageTimer;
		// Phase One Spells (Ground)
		uint32 SunderArmorTimer;
		uint32 DragonBreathTimer;
		uint32 DragonClawTimer;
		// Phase Two Spells (Air)
		uint32 FireBreathTimer;
		uint32 DragonBerserkTimer;
		uint32 DragonFireStrikeTimer;
		// Phase Three Spells (Ground)
		uint32 DragonShieldTimer;
		// Phase Filters
		bool Phase1;
		bool Phase2;
		bool Phase3;
		bool InFlight;
		// Spell Filters
		bool IsChanneling;
		bool Enraged;
		//bool buffCheck;

		void Reset()
		{
			me->SetCanFly(false);
			me->SetDisableGravity(false);
			me->RemoveAllAuras();
			me->SetFullHealth();
			Phase1 = false;
			Phase2 = false;
			Phase3 = false;
			InFlight = false;
			IsChanneling = false;
			Enraged = false;
			//buffCheck = true;
			// Enraged Spells
			FingerOfDeath_Timer = 0;
			EnrageTimer = 1200000; //20 min
			// Phase One Spells (Ground)
			SunderArmorTimer = 10000;
			DragonBreathTimer = 25000;
			DragonClawTimer = 15000;
			// Phase Two Spells (Air)
			FireBreathTimer = 45000;
			DragonBerserkTimer = 60000;
			DragonFireStrikeTimer = 15000;
			// Phase Three Spells (Ground)
			DragonShieldTimer = 0;
		}

		void JustReachedHome()
        {
            me->RemoveAllAuras();
			me->SetDisableGravity(false);
			me->SetCanFly(false);
			SetCombatMovement(true);
			InFlight = false;
			Phase1 = false;
			Phase2 = false;
			Phase3 = false;
			//buffCheck = true;
        }
		
		void EnterCombat(Unit* who)
		{
			Talk(SOUND_ONAGGRO);
			me->RemoveAllAuras();
			DoZoneInCombat();
			Phase1 = true;
			//me->CastSpell(me, 26662, true);
			//buffCheck = true;
		}

		void KilledUnit(Unit* /*who*/)
		{
			Talk(SOUND_ONSLAY);
		}

		void JustDied(Unit* /*killer*/)
		{
			Talk(SOUND_ONDEATH);
		}

		void UpdateAI(uint32 diff)
		{
			if (!UpdateVictim())
				return;

			if (HealthBelowPct(66))
			{
				Phase1 = false;
				Phase2 = true;
				Phase3 = false;
				//buffCheck = true;
			}

			if (HealthBelowPct(33))
			{
				Phase1 = false;
				Phase2 = false;
				Phase3 = true;
				//buffCheck = true;
			}

			if (HealthBelowPct(10) && !Enraged)
				Enraged = true;
				//buffCheck = true;

			if (Enraged)
			{
				DoCast(me, SPELL_BERSERK);
				//buffCheck = true;
			}

			//if (buffCheck)
				//{
						//me->CastSpell(me, 26662, true);
						//buffCheck = false;
				//}
			
			if (Phase1)
			{
				if (SunderArmorTimer <= diff)
				{
					DoCast(me->GetVictim(), SPELL_SUNDER_ARMOR);
					SunderArmorTimer = urand(20000, 40000);
				} else SunderArmorTimer -= diff;

				if (DragonClawTimer <= diff)
				{
					DoCast(me->GetVictim(), SPELL_DRAGON_CLAW);
					DragonClawTimer = urand(20000, 30000);
				} else DragonClawTimer -= diff;
				
				if (DragonBreathTimer <= diff)
				{
					DoCast(me->GetVictim(), SPELL_DRAGON_BREATH_PTWO);
					DragonBreathTimer = urand(18000, 25000);
				} else DragonBreathTimer -= diff;
			}

			if (Phase2)
			{
				if(!InFlight)
				{
				 	me->SetCanFly(true);
					me->SetDisableGravity(true);
					me->CastSpell(me, 57764, true);
					//DoCast(me, 57764, true); // animation
					me->SetSpeed(MOVE_FLIGHT, 2.0f);
					me->GetMotionMaster()->MovePoint(0, x, y, z + 15.0f);
					me->GetMotionMaster()->Clear(false);
					me->GetMotionMaster()->MoveRandom(30.0f);// MoveIdle();
					InFlight = true;
					//buffCheck = true;
				}
									
				if (FireBreathTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE);
						DoCast(Target, SPELL_CHARRED_EARTH);
						DoCast(Target, SPELL_DEATH_AND_DECAY, true); 
						//Talk(SAY_RUNAWAY);
						FireBreathTimer = urand(15000, 35000);
						}
					} else FireBreathTimer -= diff;

				if (DragonFireStrikeTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_DRAGON_ENGULF);
						DoCast(Target, SPELL_CHARRED_EARTH);
						DoCast(Target, SPELL_DEATH_AND_DECAY, true);
						//Talk(SAY_RUNAWAY);
						DragonFireStrikeTimer = urand(10000, 25000);
						}
					} else DragonFireStrikeTimer -= diff;

				if (DragonShieldTimer <= diff)
				{
					DoCast (me, SPELL_DRAGON_FIRE_SHIELD);
					DragonShieldTimer = 20000;
				} else DragonShieldTimer -= diff;
			}

			if (Phase3)
			{
				if(InFlight)
				{
					me->SetDisableGravity(false);
					me->SetCanFly(false);

					if (me->HasAura(57764))
						me->RemoveAura(57764);

					SetCombatMovement(true);
				
					me->GetMotionMaster()->MoveChase(me->GetVictim()); 
					InFlight = false;
					//buffCheck = true;
				}
							
				if (SunderArmorTimer <= diff)
				{
					DoCast(me->GetVictim(), SPELL_SUNDER_ARMOR);
					SunderArmorTimer = urand (20000,30000);
				} else SunderArmorTimer -= diff;

				if (DragonClawTimer <= diff)
				{
					DoCast(me->GetVictim(), SPELL_DRAGON_CLAW);
					DragonClawTimer = urand(20000, 30000);
				} else DragonClawTimer -= diff;
				
				if (DragonFireStrikeTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE);
						DoCast(Target, SPELL_CHARRED_EARTH);
						DoCast(Target, SPELL_DEATH_AND_DECAY, true);
						//Talk(SAY_RUNAWAY);
						DragonFireStrikeTimer = urand(25000, 85000);
						}
					} else DragonFireStrikeTimer -= diff;
				
				if (DragonShieldTimer <= diff)
				{
					DoCast (me, SPELL_DRAGON_FIRE_SHIELD);
					DragonShieldTimer = 15000;
				} else DragonShieldTimer -= diff;
			}
			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_mirxal()
{
	new boss_mirxal;
}
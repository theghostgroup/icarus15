/********************************\
*      BotCore Custom Boss       *
*   Ruarzog, Naxril, and Wixar   *
*        Type: Header File       *
*         By Josh Carter         *
\********************************/

#include "ScriptPCH.h"
using namespace std;

// Disabled for now, keep for future script expansion
/*
uint64 PlayerGUID;
bool risBattleActive = false;
bool rhasLogged = false;
bool rinZone = true;
int risWaveBossDead = 0;
*/
enum Events
{
	EVENT_NONE,
	EVENT_BATTLE_END,
	EVENT_PHASE_ONE, // Dialogues
	EVENT_PHASE_TWO, // Ruarzog OOC -> Fight Naxril & Wixar (Enrage if One dies)
	EVENT_PHASE_THREE, // Tank and spank (5 min timer until boss kills all)
};

enum Phases
{
	PHASE_START_COMBAT,
	PHASE_END_COMBAT,
	PHASE_END,
};

// Spell info found on http://wotlk.openwow.com
enum SpellIds
{
	// DAMAGE SPELLS
	// Ruarzog:
	SPELL_DRAGON_MASSIVE_FLAMESTRIKE 	= 26558, // For Visual Effect
	SPELL_SOUL_REAPER					= 69409, // Deals 70000 Shadow damage and the caster gains 100% increased haste after 5 sec.
	SPELL_CHARRED_EARTH					= 30129, // Chars the ground, setting it flame, dealing 2188 - 2812 Fire damage every 3 sec to the inflamed area over 30 sec.
	SPELL_RAIN_OF_CHAOS					= 71965, // 3420 - 3780 Chaos damage inflicted every 5 sec. (AOE)
	SPELL_LAVA_BOMB						= 20474, // Throws a Lava Bomb at an enemy, inflicting 3200 Fire damage over 8 sec.

	// Naxril:
	SPELL_METEOR_STRIKE					= 75879, // Deals 75000 - 85000 Fire damage to enemies within 12 yards and spawns a Living Inferno. (NPC ID 40681)
	SPELL_METEOR_FISTS					= 67331, // Inflicts 125% Fire weapon damage split with a nearby target.
	SPELL_EARTH_FURY					= 54193, // Throws all nearby enemies into the air, dealing damage and slowing their movement speed for 6 sec.
	SPELL_JUMP_ATTACK					= 56113, // Jumps at the target, inflicting 120% weapon damage.
	SPELL_DEATH_GRIP					= 64431, // Grips the target in the air and damages (minor damage)

	//Wixar:
	SPELL_SINFUL_BEAM					= 40827, // Strikes an enemy with shadow energy that arcs to another nearby enemy. The spell affects up to 10 targets.
	SPELL_CHAINLIGHTNING				= 49271,
	SPELL_BOOST_CHAINLIGHTNING			= 26123, // +5%
	SPELL_INCREASE_NATURE_DAMAGE		= 38305, // +66
	SPELL_NATURE_ALIGNED				= 23734, // Spell power increased by 250. Mana cost increased by 20%. (20 sec duration)
	SPELL_MOONFIRE						= 67945, // Burns the enemy for 2760 - 3240 Arcane damage and then an additional 6000 Arcane damage over 12 sec.
	//Enrage:
	SPELL_FRENZY  			            = 48017, // Increases the caster's Physical damage by 100%.
	SPELL_DRAIN_POWER					= 59355, // Drains the power out of all nearby enemies, reducing their damage done by 3% and increasing your damage done by 6%.
	SPELL_FURY_OF_FROSTMOUNE			= 70063, // Death, Deals 1000000 Shadow damage to all enemies.
	SPELL_MASTER_BUFF_MELEE				= 35874, // Master Buff (Physical)
	SPELL_MASTER_BUFF_MAGIC				= 35912, // Master Buff (Magical)
	SPELL_MASTER_BUFF_RANGE				= 38734, // Master Buff (Ranged)
	SPELL_ASHBRINGER_FURY				= 28754, // Strength increased by 300.
	SPELL_FRENZYHEART_FURY				= 59821, // Your critical strike rating is increased by 91. (10 SECONDS)

	//MISC:
	SPELL_BONE_SHIELD					= 27688,
};

enum SpawnIds
{
	NPC_RUARZOG_BOSS					= 808080,
	NPC_NAXRIL_BOSS						= 808082,
	NPC_WIXAR_BOSS						= 808081,
	GOBJECT_RUARZOG_BOSS_RUNE			= 183036,
};

enum IsWhere
{
	KARAZHAN_ZONE = 3457,
	KARAZHAN_AREA = 3457,
};

struct RuarzogMeteor
{
	uint32 gobject;
	uint32 spellId;
	float x, y, z, o;
};

static RuarzogMeteor sMoveData[] =
{
	{/*GOBJECT_RUARZOG_BOSS_RUNE,*/ SPELL_DRAGON_MASSIVE_FLAMESTRIKE, SPELL_CHARRED_EARTH, -10989.604f, -1911.566f, 78.868f, 4.583f } // Need New Coords
};

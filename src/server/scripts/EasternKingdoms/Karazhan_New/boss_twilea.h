/*
Boss Twilea Header File
Custom Boss Encounter for Karazhan_New
335a BotCore Funserver
By: Johnathan J. Carter
*/

uint64 T_PlayerGUID;

enum Yells // Need to look through creature text and get decent text and sound ID's then write boss_sounds sql for this
{
SAY_AGGRO			= 0,
SAY_BOSS_DEATH		= 2,
SAY_PLAYER_DEATH	= 1,	
};

enum eEnums
{
KARAZHAN_ZONE = 3457,
KARAZHAN_AREA = 3457,
};

enum SpawnIds
{
/* Adds Waves */

NPC_T_PORTAL 				= 85007, 	
/* SPIDERS */
/* Right Side */
NPC_SPIDER_ONE				= 85008,	 
NPC_SPIDER_TWO				= 85009,
NPC_SPIDER_THREE			= 85010,
/*Left Side */
NPC_SPIDER_FOUR				= 85011,
NPC_SPIDER_FIVE				= 85012,
NPC_SPIDER_SIX				= 85013,

/* Large Spiders */
NPC_LARGESPIDER_ONE			= 85014,
NPC_LARGESPIDER_TWO			= 85015,

/* END GAME */
NPC_SCARAB_ONE				= 85016,	
NPC_SCARAB_TWO				= 85017,	

/* BOSS */
BOSS_TWILEA					= 85018,
BOSS_SCARAB_RISEN_ONE		= 85019,
BOSS_SCARAB_RISEN_TWO		= 85020,
};

enum SpellIds
{
/* Boss Spells */
SPELL_SHADOW_STORM 			= 2148,  // Periodically casts Shadow Storm.
SPELL_WEB_SPRAY				= 55509, // 40 sec internal cd, cast every 45 sec @ random target.
SPELL_SINBEAM				= 40827,
/* Scarab Spells */
SPELL_METEOR_FISTS			= 67331, // Inflicts 125% Fire weapon damage split with a nearby target.
SPELL_FRENZY                = 8269,	 // Increase Haste
SPELL_SOUL_REAPER			= 69409,
/* Spider Spells */
SPELL_SPIDER_KISS			= 17332, // [Aura]Chance on Hit: Immobilizes the target and lowers their armor by 100 for 10 sec.
SPELL_POISON_VOLLY			= 54098, // Shoots poison at all enemies, inflicting 3375 - 4125 Nature damage, then additional damage every 5 sec. for 8 sec.
};

#define MAX_WAVE_SPAWN_LOCATIONS 12
const uint32 T_waveList[MAX_WAVE_SPAWN_LOCATIONS] =
{
/* First Wave Ids */
NPC_SPIDER_ONE, NPC_SPIDER_TWO, NPC_SPIDER_THREE,		 
/* Second Wave Ids */
NPC_SPIDER_FOUR, NPC_SPIDER_FIVE, NPC_SPIDER_SIX, 		 
/* Large Spider Ids */
NPC_LARGESPIDER_ONE, NPC_LARGESPIDER_TWO,				
/* Scarab Ids */
NPC_SCARAB_ONE, NPC_SCARAB_TWO,
/* Risen Scarab Ids */
BOSS_SCARAB_RISEN_ONE, BOSS_SCARAB_RISEN_TWO,								 	
};

static Position T_waveSpawns[] =
{
/*    X               Y            Z           O      */
/* Spawn Location */
{ -10981.106f, -1912.649f, 78.869f, 2.979f }, // Portal Location T_waveSpawns[0] /* Left Side of Room */
{ -10993.492f, -1911.247f, 78.869f, 6.152f }  // Portal Location T_waveSpawns[1] /* Right Side of Room */
};







/*
ScriptName: Soul Thief Spy
SD Complete: 100%
Written by JJ Carter
© Icarus Gaming Inc. 2013-2016. All rights reserved. 
For Soul Thief 255 Funserver
*/

#include "ScriptPCH.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum Spells
{
	SPELL_STUN				= 56,
};

enum SummonedNpc
{
	NPC_PORTAL				= 86001,
	NPC_ST_SPY				= 86002,
	NPC_QUEST_GIVER			= 666805,	
};

class npc_st_spy : public CreatureScript
{
public:
	npc_st_spy() : CreatureScript("npc_st_spy") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_st_spyAI (creature);
	}

	struct npc_st_spyAI : public ScriptedAI
	{
		npc_st_spyAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me) { }
		
		char msg[200];
		SummonList Summons;
		bool Fled;
		Unit* my_target;
				
		void Reset()
		{
			Summons.DespawnAll();
			
			if (!IsCombatMovementAllowed())
				SetCombatMovement(true);
			Fled = false;
			me->RemoveAllAuras();
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 36556); // Apocalyptic Staff
		}

		void JustReachedHome()
        {
			Fled = false;
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_AGGRESSIVE);
			return;
        }
		
		void KilledUnit(Unit* victim) OVERRIDE
		{
			me->MonsterYell("Ha! Another Victory for the Soul Thief!", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("Impossible!!!", LANG_UNIVERSAL, me);
		}
		
		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("So you've found me... Now what little hero...", LANG_UNIVERSAL, me);
		}
		
		void UpdateAI(uint32 diff) OVERRIDE
		{
			if (!UpdateVictim())
				return;
			
			if (me->HasUnitState(UNIT_STATE_CASTING)) // return when casting
				return;
			
			if (HealthBelowPct(30) && !Fled)
			{
				DoCast(me->GetVictim(), SPELL_STUN, true);
				me->SummonCreature(NPC_PORTAL, me->GetHomePosition(), TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 5000);
				Fled = true;
			}
			
			if (Fled)
			{
				if (my_target = SelectTarget(SELECT_TARGET_TOPAGGRO,0))
				{
					me->MonsterYell("You cannot hope to beat Mirxal'Algothal. The Soul Thief shall prevail", LANG_UNIVERSAL, me);
					sprintf(msg,"%s stuns %s then summons a portal and disappears",me->GetName().c_str(), my_target->GetName().c_str());	
					me->MonsterTextEmote(msg, my_target);
					me->SummonCreature(NPC_QUEST_GIVER, me->GetHomePosition(), TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 60000);
					me->DespawnOrUnsummon(1);
					me->DisappearAndDie();
						return;
					Fled = false;
				}	
			}
		DoMeleeAttackIfReady();
		}
	};	
};

class npc_spy_portal : public CreatureScript
{
public:
	npc_spy_portal() : CreatureScript("npc_spy_portal") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_spy_portalAI (creature);
	}

	struct npc_spy_portalAI : public ScriptedAI
	{
		npc_spy_portalAI(Creature* pCreature) : ScriptedAI(pCreature) {}

	
		void Reset()
		{
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
		}
	};	
};

void AddSC_npc_st_spy()
{
new npc_st_spy;
new npc_spy_portal;
}

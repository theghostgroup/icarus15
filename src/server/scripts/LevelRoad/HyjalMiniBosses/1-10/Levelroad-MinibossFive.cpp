/*
**********************************
*      Icarus-Gaming Inc.        *
*  MiniBossFive Level Road IA    *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 50 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_BREATH_OF_FIRE					= 12257, // Deals 50 Fire damage every 1 sec for 5 sec to all enemies in front of you. Gets you quite drunk too!
SPELL_HELLFIRE							= 11684, // Ignites the area surrounding the caster, causing 208 Fire damage to himself and 208 Fire damage to all nearby enemies every 1 sec. Lasts 15 sec.
SPELL_RAIN_OF_FIRE						= 11677, // Calls down a fiery rain to burn enemies in the area of effect for 220 Fire damage over 8 sec.
SPELL_SOUL_FIRE							= 6353,  // Burn the enemy's soul, causing 623 - 783 Fire damage.
SPELL_FROST_FIRE_BOLT					= 69984, // Launches a bolt of frostfire at the enemy, causing 600 - 1000 Frost-Fire damage, slowing movement speed by 40% and causing an additional 900 Fire damage over 9 sec. If the target is more vulnerable to Frost damage, this spell will cause Frost instead of Fire damage.
};

class npc_minibossfive : public CreatureScript
{
public:
	npc_minibossfive() : CreatureScript("npc_minibossfive") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibossfiveAI (creature);
	}

	struct npc_minibossfiveAI : public ScriptedAI
	{
		npc_minibossfiveAI(Creature* creature) : ScriptedAI(creature) {}
		
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 BreathOfFireTimer;
			uint32 HellFireTimer;
			uint32 RainOfFireTimer;
			uint32 SoulFireTimer;
			uint32 FrostFireBoltTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			BreathOfFireTimer = urand(45000, 60000);
			HellFireTimer = 20000;
			RainOfFireTimer = 30000;
			SoulFireTimer = 45000;
			FrostFireBoltTimer = 60000; 
			MiniBossInCombat = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 944); // Elemental Mage Staff
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Burn in damnation little hero!!!", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("No... how can it be... I Perish!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("HAHAHA, fools, all of you!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("So much to burn... So little time!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000);
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("All of this land will perish under the Soul Thief's rule!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); 
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("La Da De Dum... I'm bored!!! I need something to BURN!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					DoCast(me, SPELL_HELLFIRE, true);
					TalkTimerThree = urand(25000, 80000); 
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (BreathOfFireTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_BREATH_OF_FIRE, true);
						me->MonsterYell("Feel the flames of my hatred!!!", LANG_UNIVERSAL, me);
						BreathOfFireTimer = urand(30000, 45000);
						}
					}else BreathOfFireTimer -= diff;
				
				if (HellFireTimer <= diff)
					{
					DoCast(me, SPELL_HELLFIRE, true);
					me->MonsterYell("All Shall Feel Hateful Flames!!!", LANG_UNIVERSAL, me);
					HellFireTimer = urand(50000, 65000);
					}else HellFireTimer -= diff;
					
				if (RainOfFireTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0)) 
						{
						DoCast(Target, SPELL_RAIN_OF_FIRE, true);
						me->MonsterYell("I will rain down my vengeance on you little 'Heroes'!!!", LANG_UNIVERSAL, me);
						RainOfFireTimer = urand(30000, 45000);
						}
					}else RainOfFireTimer -= diff;
									
					if (SoulFireTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0)) 
						{
						DoCast(Target, SPELL_SOUL_FIRE, true);
						me->MonsterYell("You there, you need to die!!!", LANG_UNIVERSAL, me);
						SoulFireTimer = urand(45000, 60000);
						}
					}else SoulFireTimer -= diff;
					
					if (FrostFireBoltTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0)) 
						{
						DoCast(Target, SPELL_FROST_FIRE_BOLT, true);
						me->MonsterYell("You there, you need to die!!!", LANG_UNIVERSAL, me);
						FrostFireBoltTimer = urand(25000, 70000);
						}
					}else FrostFireBoltTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibossfive()
{
	new npc_minibossfive();
}		
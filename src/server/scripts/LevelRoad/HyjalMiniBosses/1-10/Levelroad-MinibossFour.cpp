/*
**********************************
*      Icarus-Gaming Inc.        *
*   MiniBossFour Level Road IA   *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 40 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_BLIZZARD						= 58693, // Calls down a blizzard that lasts 8 sec., inflicting 1500 Frost damage every 2 sec. and slowing the movement speed of all enemies in a selected area by 40%.
SPELL_BLISTERING_COLD				= 70123, // Deals 30000 Frost damage to enemies within 25 yards.
SPELL_CONE_OF_COLD					= 27087, // Targets in a cone in front of the caster take [410 - 448*] to [410 - 448*] Frost damage and are slowed by 50% for 8 sec.
SPELL_CONSUMING_SHADOWS				= 17716, // Deals 200 Shadow damage and reduces movement speed by 30% for 1.5 sec.
SPELL_FROST_HOLD					= 10017, // Blasts enemies near the caster for 47 - 53 cold damage and freezes them in place for up to 10 sec.
};

class npc_minibossfour : public CreatureScript
{
public:
	npc_minibossfour() : CreatureScript("npc_minibossfour") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibossfourAI (creature);
	}

	struct npc_minibossfourAI : public ScriptedAI
	{
		npc_minibossfourAI(Creature* creature) : ScriptedAI(creature) {}
		
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 BlizzardTimer;
			//uint32 BlisteringColdTimer;
			uint32 ConeOfColdTimer;
			uint32 OutColdTimer;
			uint32 FrostHoldTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			BlizzardTimer = urand(45000, 60000);
			//BlisteringColdTimer = 20000;
			ConeOfColdTimer = 30000;
			OutColdTimer = 45000;
			FrostHoldTimer = 60000; 
			MiniBossInCombat = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 77949); // Spell Dagger Cata: Golad
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Hmmm... He'll be lunch later...", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("I feel the cold hands of death!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Ahh, little adventurers that yearn for an icy grave!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("When I took this job... They didn't tell me about all these overtime hours...!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000);
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("The Soul Thief will rule ALL!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); 
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("La Da De Dum... I'm bored!!! I need something to FREEZE!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					DoCast(me, SPELL_BLISTERING_COLD, true);
					TalkTimerThree = urand(25000, 80000); 
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (BlizzardTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_BLIZZARD, true);
						me->MonsterYell("Ice and Rain, MUHAHAHA!!!", LANG_UNIVERSAL, me);
						BlizzardTimer = urand(30000, 45000);
						}
					}else BlizzardTimer -= diff;
				
				/*if (BlisteringColdTimer <= diff)
					{
					DoCast(me, SPELL_BLISTERING_COLD, true);
					me->MonsterYell("All Shall Feel My Power!!!", LANG_UNIVERSAL, me->GetGUID());
					BlisteringColdTimer = urand(50000, 65000);
					}else BlisteringColdTimer -= diff;*/
					
				if (ConeOfColdTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_CONE_OF_COLD, true);
					me->MonsterYell("I have a cold! *Cough* Oh sorry, that got on you...", LANG_UNIVERSAL, me);
					ConeOfColdTimer = urand(45000, 75000);
					}else ConeOfColdTimer -= diff;
					
			if(OutColdTimer <= diff)
			{
				if (Unit* Target = SelectTarget (SELECT_TARGET_BOTTOMAGGRO, 0)) // Should Hit Weakest Target
				{
					DoCast(Target, SPELL_CONSUMING_SHADOWS, true);
					me->MonsterYell("You are WEAK, You won't notice this much!!!", LANG_UNIVERSAL, me);
					OutColdTimer = urand(50000, 70000);
				}
			} else OutColdTimer -= diff;					
					
					if (FrostHoldTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_FROST_HOLD, true);
					me->MonsterYell("Stand still everyone so I can hit you!!!", LANG_UNIVERSAL, me);
					FrostHoldTimer = urand(45000, 75000);
					}else FrostHoldTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibossfour()
{
	new npc_minibossfour();
}		
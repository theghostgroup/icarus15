/*
**********************************
*      Icarus-Gaming Inc.        *
*	MiniBossOne Level Road IA    *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 10 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_LIGHTNING_BOLT		= 6041, // Rank 6
SPELL_CHAINLIGHTNING		= 45298, // Rank 2
SPELL_LIGHTNING_BREATH		= 25012, // Rank 6
SPELL_NATURES_GRACE         = 16886, // Spell casting speed increased by 20% for 3 sec.
};

class npc_minibossone : public CreatureScript
{
public:
	npc_minibossone() : CreatureScript("npc_minibossone") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibossoneAI (creature);
	}

	struct npc_minibossoneAI : public ScriptedAI
	{
		npc_minibossoneAI(Creature* creature) : ScriptedAI(creature) {}
	
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 LightningBoltTimer;
			uint32 ChainLightningTimer;
			uint32 LightningBreathTimer;
			uint32 NaturesGraceTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			LightningBoltTimer = urand (45000, 90000);
			ChainLightningTimer = urand(15000, 25000);
			LightningBreathTimer = urand(35000, 65000);
			NaturesGraceTimer = 3000; // 3 seconds last 3 seconds
			MiniBossInCombat = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 2013); // Crypt Bone Staff
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Yup... He Gone... ", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("Oh... I didn't know it would hurt this bad...", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Ahhh... A little group of heroes for me to eat!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("I wonder where all the heroes are... I'd like to eat one... or two...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000); // Changed to 1 Min
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("I'm so bored... I need a new job... I wonder if my union rep is around today...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); // Changed to 1 Min
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("Maybe I'll just wander off for a bit and go to that nice restaurant down in ShadowPan that we took over yesterday...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = urand(25000, 80000); // Changed to 1 Min
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (NaturesGraceTimer <= diff)
					{
					DoCast(me, SPELL_NATURES_GRACE, true);
					NaturesGraceTimer = urand(3000, 10000);
					}
					else NaturesGraceTimer -= diff;
				
				if (LightningBoltTimer <= diff)
					{
					DoCastVictim(SPELL_LIGHTNING_BOLT, true);
					me->MonsterYell("Feel my strength!!!", LANG_UNIVERSAL, me);
					LightningBoltTimer = urand(50000, 65000);
					}
					else LightningBoltTimer -= diff;
					
				if (LightningBreathTimer <= diff)
					{
					DoCastVictim(SPELL_LIGHTNING_BREATH, true);
					me->MonsterYell("I'll spit lighting on you!!! HAHAHA!!!", LANG_UNIVERSAL, me);
					LightningBreathTimer = urand(50000, 65000);
					}
					else LightningBreathTimer -= diff;	
									
				if (ChainLightningTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_CHAINLIGHTNING);
						me->MonsterYell("Stand a little closer together for me!!!", LANG_UNIVERSAL, me);
						ChainLightningTimer = urand(30000, 45000);
						}
					}
					else ChainLightningTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibossone()
{
	new npc_minibossone();
}		
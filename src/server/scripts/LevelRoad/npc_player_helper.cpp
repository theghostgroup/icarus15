/*
* NPC Server Helper
* By Josh Carter & Matthew Ferrill
* Only for Icarus-Gaming
* © 2013 - 2016 Icarus Gaming Inc.
*/

/* 
ToDO:
Add Missing Item Sets
*/

#include "ScriptPCH.h"
#include "Config.h"
#include "npc_player_helper.h"

enum MenuStructure
{
	MAIN_MENU      		= 1,
	HYJAL_MENU		  	= 2,
	PANDARIA_MENU		= 3,
	VASHJIR_MENU		= 4,
	PL_MENU				= 5,
	PEQ_MENU			= 6,
	LRO_MENU			= 7,
	PEQMAX_MENU			= 8,
};

class npc_playerhelper : public CreatureScript
{
public:
	npc_playerhelper() : CreatureScript("npc_playerhelper") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();

		if (sConfigMgr->GetBoolDefault("Server.PlayerHelper.Main.Enable", true))
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Level Road Options->", GOSSIP_SENDER_MAIN, LRO_MENU);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Player Level Up Options->", GOSSIP_SENDER_MAIN, PL_MENU);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Player Starter Equipment Packages->", GOSSIP_SENDER_MAIN, PEQ_MENU);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Player MaxLevel Equipment Packages->", GOSSIP_SENDER_MAIN, PEQMAX_MENU);
			player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
			return true;
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{
		case LRO_MENU:
			if (sConfigMgr->GetBoolDefault("Server.Levelroad.Option.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Take me to Mount Hyjal.", GOSSIP_SENDER_MAIN, ACTION_GO_HYJAL);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Take me to Pandaria.", GOSSIP_SENDER_MAIN, ACTION_GO_PANDARIA);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Take me to Vashj'ir.", GOSSIP_SENDER_MAIN, ACTION_GO_VASHJIR);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "Not Now! I'm not ready!", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;

		case ACTION_GO_HYJAL:
			player->CLOSE_GOSSIP_MENU();
			player->TeleportTo(1, 5538.133f, -3628.944f, 1566.483f, 2.101f);
			break;

		case ACTION_GO_PANDARIA:
			player->CLOSE_GOSSIP_MENU();
			creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			break;

		case ACTION_GO_VASHJIR:
			player->CLOSE_GOSSIP_MENU();
			creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			break;
			
		case PL_MENU:
			if (sConfigMgr->GetBoolDefault("Server.LevelMax.Option.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "Make me level 255. I'm ready!", GOSSIP_SENDER_MAIN, ACTION_LEVEL_MAX);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "No, I prefer to start at level 1.", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;	
				
		case ACTION_LEVEL_MAX:
			player->CLOSE_GOSSIP_MENU();
			player->GiveLevel(255);
			break;	
		
		case PEQ_MENU:
			if (sConfigMgr->GetBoolDefault("Server.CStartGear.Option.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Warrior Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_WARRIOR);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Paladin Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_PALLY);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Priest Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_PRIEST);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Rogue Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_ROGUE);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Hunter Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_HUNTER);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Shaman Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_SHAMMY);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Druid Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_DRU);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Mage Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_MAGE);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Warlock Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_WARLOCK);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Death Knight Starter Set", GOSSIP_SENDER_MAIN, ACTION_STARTER_DK);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Wait, I need to make room in my bags!!!", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;	
		
		case ACTION_STARTER_WARRIOR:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MELEE_STARTER_SWORD, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_BRACERS, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_SABATONS, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_GUANTLETS, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_BELT, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_BREASTPLATE, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_HELM, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_LEGPLATES, 1);
			player->AddItem(ITEM_WARRIOR_STARTER_PAULDRONS, 1);
			break;	
		
		case ACTION_STARTER_PALLY:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_PALADIN_STARTER_BRACERS, 1);
			player->AddItem(ITEM_PALADIN_STARTER_GUANTLETS, 1);
			player->AddItem(ITEM_PALADIN_STARTER_CHESTGUARD, 1);
			player->AddItem(ITEM_PALADIN_STARTER_HELM, 1);
			player->AddItem(ITEM_PALADIN_STARTER_LEGPLATES, 1);
			player->AddItem(ITEM_PALADIN_STARTER_PAULDRONS, 1);
			player->AddItem(ITEM_PALADIN_STARTER_BOOTS, 1);
			player->AddItem(ITEM_MELEE_STARTER_SWORD, 1);
			break;
			
		case ACTION_STARTER_PRIEST:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MAGIC_STARTING_WAND, 1);
			player->AddItem(ITEM_ALL_STARTING_STAFF, 1);
			player->AddItem(ITEM_PRIEST_STARTER_VAMBRACES, 1);
			player->AddItem(ITEM_PRIEST_STARTER_BOOTS, 1);
			player->AddItem(ITEM_PRIEST_STARTER_GLOVES, 1);
			player->AddItem(ITEM_PRIEST_STARTER_GIRDLE, 1);	
			player->AddItem(ITEM_PRIEST_STARTER_ROBES, 1);
			player->AddItem(ITEM_PRIEST_STARTER_CIRCLET, 1);
			player->AddItem(ITEM_PRIEST_STARTER_PANTS, 1);
			player->AddItem(ITEM_PRIEST_STARTER_MANTLE, 1);
			break;	
		
		case ACTION_STARTER_ROGUE:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MELEE_STARTER_DAGGER, 2);
			player->AddItem(ITEM_ROGUE_STARTER_BRACERS, 1);
			player->AddItem(ITEM_ROGUE_STARTER_BOOTS, 1);
			player->AddItem(ITEM_ROGUE_STARTER_GLOVES, 1);
			player->AddItem(ITEM_ROGUE_STARTER_BELT, 1);
			player->AddItem(ITEM_ROGUE_STARTER_CHESTPIECE, 1);
			player->AddItem(ITEM_ROGUE_STARTER_COVER, 1);
			player->AddItem(ITEM_ROGUE_STARTER_PANTS, 1);
			player->AddItem(ITEM_ROGUE_STARTER_SHOULDERPADS, 1);
			break;
		
		case ACTION_STARTER_HUNTER:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MELEE_STARTER_SWORD, 1);
			player->AddItem(ITEM_RANGE_STARTING_BOW, 1);
			player->AddItem(ITEM_HUNTER_STARTER_BRACERS, 1);
			player->AddItem(ITEM_HUNTER_STARTER_BOOTS, 1);
			player->AddItem(ITEM_HUNTER_STARTER_GLOVES, 1);
			player->AddItem(ITEM_HUNTER_STARTER_BELT, 1);
			player->AddItem(ITEM_HUNTER_STARTER_BREASTPLATE, 1);
			player->AddItem(ITEM_HUNTER_STARTER_HELMET, 1);
			player->AddItem(ITEM_HUNTER_STARTER_LEGGINGS, 1);
			player->AddItem(ITEM_HUNTER_STARTER_EPAULETS, 1);
			break;
		
		case ACTION_STARTER_SHAMMY:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MAGIC_STARTING_WAND, 1);
			player->AddItem(ITEM_ALL_STARTING_STAFF, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_BRACERS, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_BOOTS, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_GAUNTLETS, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_BELT, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_VESTMENTS, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_HELMET, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_LEGGINGS, 1);
			player->AddItem(ITEM_SHAMMY_STARTER_EPAULETS, 1);
			break;
		
		case ACTION_STARTER_DRU:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MAGIC_STARTING_WAND, 1);
			player->AddItem(ITEM_ALL_STARTING_STAFF, 1);
			player->AddItem(ITEM_DRUID_STARTER_BRACERS, 1);
			player->AddItem(ITEM_DRUID_STARTER_BOOTS, 1);
			player->AddItem(ITEM_DRUID_STARTER_GLOVES, 1);
			player->AddItem(ITEM_DRUID_STARTER_BELT, 1);
			player->AddItem(ITEM_DRUID_STARTER_VESTMENTS, 1);
			player->AddItem(ITEM_DRUID_STARTER_HELM, 1);
			player->AddItem(ITEM_DRUID_STARTER_LEGGINGS, 1);
			player->AddItem(ITEM_DRUID_STARTER_SPAULDERS, 1);
			break;
		
		case ACTION_STARTER_MAGE:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MAGIC_STARTING_WAND, 1);
			player->AddItem(ITEM_ALL_STARTING_STAFF, 1);
			player->AddItem(ITEM_MAGE_STARTER_BINDINGS, 1);
			player->AddItem(ITEM_MAGE_STARTER_BOOTS, 1);
			player->AddItem(ITEM_MAGE_STARTER_GLOVES, 1);
			player->AddItem(ITEM_MAGE_STARTER_BELT, 1);
			player->AddItem(ITEM_MAGE_STARTER_ROBES, 1);
			player->AddItem(ITEM_MAGE_STARTER_CROWN, 1);
			player->AddItem(ITEM_MAGE_STARTER_LEGGINGS, 1);
			player->AddItem(ITEM_MAGE_STARTER_MANTLE, 1);
			break;
			
		case ACTION_STARTER_WARLOCK:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MAGIC_STARTING_WAND, 1);
			player->AddItem(ITEM_ALL_STARTING_STAFF, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_BRACERS, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_SLIPPERS, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_GLOVES, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_BELT, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_ROBES, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_HORNS, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_PANTS, 1);
			player->AddItem(ITEM_WARLOCK_STARTER_SHOULDER, 1);
			break;	
		
		case ACTION_STARTER_DK:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			player->AddItem(ITEM_MELEE_STARTER_SWORD, 1);
			player->AddItem(ITEM_DK_STARTER_WRISTGUARD, 1);
			player->AddItem(ITEM_DK_STARTER_GREAVES, 1);
			player->AddItem(ITEM_DK_STARTER_GAUNTLETS, 1);
			player->AddItem(ITEM_DK_STARTER_GIRDLE, 1);
			player->AddItem(ITEM_DK_STARTER_TUNIC, 1);
			player->AddItem(ITEM_DK_STARTER_HOOD, 1);
			player->AddItem(ITEM_DK_STARTER_LEGGPLATES, 1);
			player->AddItem(ITEM_DK_STARTER_PAULDRONS, 1);
			break;
		
		case PEQMAX_MENU:
			if (sConfigMgr->GetBoolDefault("Server.CMLGear.Option.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Warrior MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_WARRIOR);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Paladin MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_PALLY);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Priest MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_PRIEST);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Rogue MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_ROGUE);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Hunter MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_HUNTER);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Shaman MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_SHAMMY);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Druid MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_DRU);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Mage MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_MAGE);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Warlock MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_WARLOCK);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Give Me The: Death Knight MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_DK);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Wait, I need to make room in my bags!!!", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;	
		
		case ACTION_MAXG_WARRIOR:
			if (sConfigMgr->GetBoolDefault("Server.CMGWarrior.Option.Enable", true))
			{
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Tank: Warrior MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_WARRIOR_TANK);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "DPS:  Warrior MaxLevel Set", GOSSIP_SENDER_MAIN, ACTION_MAXG_WARRIOR_DPS);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Wait, I need to make room in my bags!!!", GOSSIP_SENDER_MAIN, ACTION_NOT_YET);
				player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE,creature->GetGUID());
				return true;
			}
			else
			{
				player->CLOSE_GOSSIP_MENU();
				creature->MonsterTextEmote(EMOTE_FUNCTION_OFF, 0, true);
			}
			break;	
		
		case ACTION_MAXG_WARRIOR_TANK:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;

		case ACTION_MAXG_WARRIOR_DPS:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;				
		
		case ACTION_MAXG_PALLY:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
			
		case ACTION_MAXG_PRIEST:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;	
		
		case ACTION_MAXG_ROGUE:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
		
		case ACTION_MAXG_HUNTER:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
		
		case ACTION_MAXG_SHAMMY:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
		
		case ACTION_MAXG_DRU:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
		
		case ACTION_MAXG_MAGE:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
			
		case ACTION_MAXG_WARLOCK:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;	
		
		case ACTION_MAXG_DK:
			player->CLOSE_GOSSIP_MENU();
			player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1);
			break;
				
		case ACTION_NOT_YET:
			player->CLOSE_GOSSIP_MENU();
			break;		
		
		case MAIN_MENU:
			OnGossipHello(player, creature);
			break;
		}
		return true;
	}
};

void AddSC_npc_playerhelper()
{
	new npc_playerhelper();
}
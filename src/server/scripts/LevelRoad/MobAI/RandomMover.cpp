#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

class npc_random_mover : public CreatureScript
{
public:
	npc_random_mover() : CreatureScript("npc_random_mover") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_random_moverAI (creature);
	}

	struct npc_random_moverAI : public ScriptedAI
	{
		npc_random_moverAI(Creature* creature) : ScriptedAI(creature) {}
	
			uint32 MoverOneTimer;
			uint32 MoverTwoTimer;
			uint32 MoverThreeTimer;
			bool MoverOne;
			bool MoverTwo;
			bool MoverThree;
			bool InCombat;
							
		void Reset()
		{
			MoverOneTimer = 0; // 35 to 40 seconds
			MoverTwoTimer = 0; // 15 to 30 seconds
			MoverThreeTimer = 0; // 45 to 60 seconds
			MoverOne = true;
			MoverTwo = false;
			MoverThree = false;
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE { }
		
		void JustDied(Unit* /*killer*/) OVERRIDE 
		{
			MoverOne = false;
			MoverTwo = false;
			MoverThree = false;
			InCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			MoverOne = false;
			MoverTwo = false;
			MoverThree = false;
			InCombat = true;
			SetCombatMovement(true);
			DoMeleeAttackIfReady();
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(MoverOne && !InCombat)
			{	
				if (MoverOneTimer <= diff)
				{
				me->GetMotionMaster()->MoveRandom(30.0f);
				MoverOneTimer = urand(20000, 40000);
				MoverOne = false;
				MoverTwo = true;
				MoverThree = false;
				} else MoverOneTimer -= diff;
			}						
			
			if (MoverTwo && !InCombat)
			{
				if (MoverTwoTimer <= diff)
				{
				me->GetMotionMaster()->MoveRandom(30.0f);
				MoverTwoTimer = urand(20000, 40000);
				MoverOne = false;
				MoverTwo = false;
				MoverThree = true;
				} else MoverTwoTimer -= diff;
			}	
			
			if (MoverThree)
			{
			if (MoverTwoTimer <= diff)
				{
				me->GetMotionMaster()->MoveRandom(30.0f);
				MoverTwoTimer = urand(20000, 40000);
				MoverOne = true;
				MoverTwo = false;
				MoverThree = false;
				} else MoverTwoTimer -= diff;
			}

			if (InCombat)
			{
				if (!UpdateVictim())
					{
					InCombat = false;
					return;
					}
				SetCombatMovement(true);
				DoMeleeAttackIfReady();
			}		
		}
	};
};

void AddSC_npc_random_mover()
{
	new npc_random_mover();
}		
/*
**********************************
*      Icarus-Gaming Inc.        *
*	  Zombie Level Road IA       *
*      Type: Zombie Mobs IA      *
*         By Josh Carter         *
**********************************
*/

/*
 * SD NOTES: 
 * Level Road Mob
 * Levels 1-5 & 6-10
 * 100% Complete
 */
 
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum ZombieSpells
{
SPELL_INFECTED_BITE = 43948,
SPELL_ZOMBIE_CLAW   = 52472, 
};

class npc_zombieattack : public CreatureScript
{
public:
	npc_zombieattack() : CreatureScript("npc_zombieattack") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_zombieattackAI (creature);
	}

	struct npc_zombieattackAI : public ScriptedAI
	{
		npc_zombieattackAI(Creature* creature) : ScriptedAI(creature) {}
	
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 InfectedBiteTimer;
			uint32 ZombieClawTimer;
			bool ZombieInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			InfectedBiteTimer = urand (45000, 90000);
			ZombieClawTimer = urand(15000, 25000);
			ZombieInCombat = false;
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Hmmmm... Brians!!!", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("arg! ... urg! ... *gurgle*", LANG_UNIVERSAL, me);
			ZombieInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Grrr... Flesh smell gooooood!!!", LANG_UNIVERSAL, me);
			ZombieInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!ZombieInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("...want... eat... flesh...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = 60000; // Changed to 1 Min
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("...grrr... grrr... *cough*", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = 60000; // Changed to 1 Min
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("...hrng... arg... brains... yummy...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = 60000; // Changed to 1 Min
					} else TalkTimerThree -= diff;
			}						
			
			if (ZombieInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (InfectedBiteTimer <= diff)
					{
					DoCastVictim(SPELL_INFECTED_BITE, true);
					me->MonsterYell("Hmmmm... tastes goooood...", LANG_UNIVERSAL, me);
					InfectedBiteTimer = urand(50000, 65000);
					}
					else
					InfectedBiteTimer -= diff;
									
				if (ZombieClawTimer <= diff)
					{
					DoCastVictim(SPELL_ZOMBIE_CLAW, true);
					me->MonsterYell("Grrrrr...  Arg...", LANG_UNIVERSAL, me);
					ZombieClawTimer = urand(30000, 45000);
					}
					else ZombieClawTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_zombieattack()
{
	new npc_zombieattack();
}		
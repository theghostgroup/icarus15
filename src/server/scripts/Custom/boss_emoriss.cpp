/*
ScriptName: Boss Emoriss
SD Complete: 100%
Added Proper Emotes
Tested 100%
Written by???? (Given to me by Mferrill)
Re-Coded by Josh Carter
*/

#include "ScriptPCH.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

//Emoriss Twilight Vanquisher
//15000k, display id : 20510
//#define SAY_AGGRO "Your curiosity will be the death of you! Prepare to perish..."
//#define SAY_KILL "Weaklings.. Why do you even bother.. You cannot hope to win!"
//#define SAY_DIE "This... this cannot be... I am ment to rule this land.. you.. cannot defeat me.."
//#define SAY_BERSERK "Enough.. The infinite dragonflight is unrivaled in power! FEEL MY WRATH!"
//#define SAY_CORRUPTINGBLIGHT "Watch as I tear your world apart!"

enum Yells
{
	SAY_AGGRO               = 0,
	SAY_KILL                = 1,
	SAY_DIE                 = 2,
	SAY_BERSERK             = 3,
	SAY_CORRUPTINGBLIGHT    = 4,
};

enum Spells
{
	REFLECTION				= 22067,		//PHASE 2    every 27 sec?
	DRAINMANA				= 46453,		//ALL PHASES, every 50 sec
	SHROUDOFSARROW			= 72982,		//Start spell
	TAILSMASH				= 71077,		// Every 40 Sec , phase 1
	SHADOWBREATH			= 59126,		// Every 30 Sec , phase 1 & 3
	STRONGCLEAVE			= 19983,		//Every 45 Sec, phase 1 & 3
	BLISTERINGCOLD		 	= 70123,		//phase 3 , 58 sec
	ON_TOUCH				= 67049,
	SHADOWBOLT				= 64698,		// phase 2, every 15 sec, random target
	CORRUPTINGBLIGHT		= 60588,        // PHASE 1 & 3 , every 25 Sec, on tank
	SPELL_BERSERK			= 62555,		// berserk 1.5%
};

enum MiniBoss
{
	TENEBRONID				= 301001, //4800k, display: 27039
	SHADRONID				= 301002, //4800k
	VESPERONID				= 301003, //4800k

	//mini bosses spells
	SPELL_SHADOW_BREATH			= 59126,

	//vesperon
	SPELL_POWER_OF_VESPERON     = 61251,
	//tenebron
	SPELL_POWER_OF_TENEBRON     = 61248,
	//shadron
	SPELL_POWER_OF_SHADRON      = 58105,
};

class boss_emoriss : public CreatureScript
{
public:
	boss_emoriss() : CreatureScript("boss_emoriss") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_emorissAI (creature);
	}

	struct boss_emorissAI : public ScriptedAI
	{
		boss_emorissAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me) {
			x = me->GetPositionX();
			y = me->GetPositionY();
			z = me->GetPositionZ();
		}
		EventMap events;
		bool has_flied;
		bool is_chargin;
		char msg[200];
		SummonList Summons;
		float x,y,z;
		Unit* charged_target;

		void Reset()
		{
			events.Reset();
			Summons.DespawnAll();
			has_flied = false;

			charged_target = 0;

			me->SetCanFly(false);
			me->SetDisableGravity(false);

			if (!IsCombatMovementAllowed())
				SetCombatMovement(true);
		}

		void KilledUnit(Unit* victim) OVERRIDE
		{
			Talk(SAY_KILL);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			Talk(SAY_DIE);
		}

		void JustSummoned(Creature* pSummoned) OVERRIDE
		{
			Summons.Summon(pSummoned);
			if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM,0,0,true))
				pSummoned->AI()->AttackStart(target);
		}

		void EnterCombat(Unit* pWho) OVERRIDE
		{
			Talk(SAY_AGGRO);
			DoCast(me, SHROUDOFSARROW);
			me->SetInCombatWithZone();

			events.ScheduleEvent(1, 10000);
			events.ScheduleEvent(2, urand(8000, 9000));
			events.ScheduleEvent(3, 3000);
			events.ScheduleEvent(4, 20000);
			events.ScheduleEvent(5, urand(2000,3000));
		}

		void MovementInform(uint32 type, uint32 id) OVERRIDE
		{
			if (type == POINT_MOTION_TYPE)
			{
				switch (id)
				{
				case 1: // start fly
					me->SetCanFly(true);
					me->SetDisableGravity(true);
					DoCast(me, 57764); // animation

					me->SetSpeed(MOVE_FLIGHT, 1.6f);
					me->GetMotionMaster()->MovePoint(0, x, y, z + 15.0f);
					me->GetMotionMaster()->Clear(false);
					me->GetMotionMaster()->MoveIdle();
					break;
				case 2: // random charge

					if (!charged_target)
						return;

					me->GetMotionMaster()->MoveCharge(charged_target->GetPositionX(), charged_target->GetPositionY(), charged_target->GetPositionZ());

					me->GetMotionMaster()->Clear(false);
					me->GetMotionMaster()->MoveIdle();
					break;
				case 3: // home
					me->SetDisableGravity(false);
					me->SetCanFly(false);

					if (me->HasAura(57764))
						me->RemoveAura(57764);

					SetCombatMovement(true);

					me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, false);
					me->ApplySpellImmune(0, IMMUNITY_EFFECT,SPELL_EFFECT_ATTACK_ME, false);
					me->GetMotionMaster()->MoveChase(me->GetVictim()); // move again?
					break;
				}
			}
		}

		void UpdateAI(uint32 uiDiff) OVERRIDE
		{
			if (!UpdateVictim())
				return;
			
			if (me->HasUnitState(UNIT_STATE_CASTING)) // return when casting
				return;

			if(me->HasAura(CORRUPTINGBLIGHT))
				me->RemoveAura(CORRUPTINGBLIGHT);

			events.Update(uiDiff);

			while (uint32 eventId = events.ExecuteEvent()) // event handling
			{
				switch (eventId)
				{
				case 1: //drain mana
					DoCast(me->GetVictim(), DRAINMANA);
					events.ScheduleEvent(1, 52000);
					break;
				case 2: // tail smash
					DoCast(TAILSMASH);
					events.ScheduleEvent(2, urand(38000, 39000));
					break;
				case 3: //shadowbreath
					DoCast(me->GetVictim(), SHADOWBREATH);
					events.ScheduleEvent(3, 20000);
					break;
				case 4: // strong cleave
					DoCast(me->GetVictim(), STRONGCLEAVE);
					events.ScheduleEvent(4, 20000);
					break;
				case 5: // corruptingblight
					DoCast(me->GetVictim(), CORRUPTINGBLIGHT);
					Talk(SAY_CORRUPTINGBLIGHT);
					events.ScheduleEvent(5, urand(15500,25500));
					break;
				case 6: // spawn shadron
					me->SummonCreature(SHADRONID, me->GetHomePosition(), TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 180000);
					events.ScheduleEvent(7, 80000);
					break;
				case 7: // spawn TENEBRON
					me->SummonCreature(TENEBRONID, me->GetHomePosition(), TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 180000);
					events.ScheduleEvent(8, 80000);
					break;
				case 8: // SPAWN vesperon
					me->SummonCreature(VESPERONID, me->GetHomePosition(), TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 180000);
					events.ScheduleEvent(9, 80000);
					break;
				case 9: //land
					me->GetMotionMaster()->MovePoint(3, me->GetHomePosition());
					is_chargin = false;
					events.Reset(); // reset all the events

					events.ScheduleEvent(3, 5000);
					events.ScheduleEvent(4, 20000);
					events.ScheduleEvent(12, urand(46000, 48000));
					events.ScheduleEvent(5, urand(2000,3000));

					break;
				case 10: //reflection
					DoCast(me, REFLECTION);
					events.ScheduleEvent(10, 12000);
					break;
				case 11: // shadowbolt
					{
						if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM,0,0,true))
							DoCast(target, SHADOWBOLT);
						events.ScheduleEvent(11, urand(12000, 14000));
					}
					break;
				case 12: // blistering COLD
					DoCast(me->GetVictim(), BLISTERINGCOLD);
					events.ScheduleEvent(12, urand(46000, 48000));
					break;
				case 13: // random charge
					if (charged_target = SelectTarget(SELECT_TARGET_RANDOM,0,0,true))
					{
						is_chargin = true;
						sprintf(msg,"%s is starting to charge at %s, Keep running!",me->GetName().c_str(), charged_target->GetName().c_str());
						me->MonsterTextEmote(msg, charged_target); // emote to all
						me->GetMotionMaster()->MovePoint(2, x, y, z + 50.0f);
						events.ScheduleEvent(13, 20000);
					}
					break;
				}
			}

			if (HealthBelowPct(65) && !has_flied)
			{
				SetCombatMovement(false);
				me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
				me->ApplySpellImmune(0, IMMUNITY_EFFECT,SPELL_EFFECT_ATTACK_ME, true);
				// Lets fly
				me->GetMotionMaster()->MovePoint(1, me->GetHomePosition());

				sprintf(msg,"%s is starting to fly!",me->GetName().c_str());
				me->MonsterTextEmote(msg, me); // emote to all
				events.Reset(); // reset all the events

				events.ScheduleEvent(6, 30000);

				events.ScheduleEvent(1, 13000);
				events.ScheduleEvent(10, 12000);
				events.ScheduleEvent(11, 5000);
				events.ScheduleEvent(13, 10000);
				has_flied = true;
			}

			if (!IsCombatMovementAllowed() && is_chargin) //in fly mode and chargin
			{
				if (!charged_target)
					return;
				if (me->isMoving()) //we still moving
					return;
				else if(me->IsInRange(charged_target,0.0f,3.5f)) //we are stopped and in range,
					DoCast(charged_target, ON_TOUCH);
				else
				{
					charged_target = 0;
					is_chargin = false;
					me->GetMotionMaster()->MovePoint(0, x, y, z + 15.0f);
					me->GetMotionMaster()->Clear(false);
					me->GetMotionMaster()->MoveIdle();
				}
			}

			if(HealthBelowPct(1.5) && !me->HasAura(SPELL_BERSERK))
			{
				Talk(SAY_BERSERK);
				DoCast(me,SPELL_BERSERK);
			}

			if (IsCombatMovementAllowed())
				DoMeleeAttackIfReady();
		}
	};
};

class boss_tenebron : public CreatureScript
{
public:
	boss_tenebron() : CreatureScript("boss_tenebron") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_tenebronAI (creature);
	}

	struct boss_tenebronAI : public ScriptedAI
	{
		boss_tenebronAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 SpellShadowbreathTimer;

		void Reset()
		{
			SpellShadowbreathTimer = 13000;
		}

		void EnterCombat(Unit* pWho) OVERRIDE
		{
			DoCast(me,SPELL_POWER_OF_TENEBRON);
		}

		void UpdateAI(uint32 uiDiff) OVERRIDE
		{
			if (SpellShadowbreathTimer < uiDiff)
			{
				DoCast(me,SPELL_SHADOW_BREATH);
				SpellShadowbreathTimer = 26000;
			}
			else
				SpellShadowbreathTimer -= uiDiff;

			DoMeleeAttackIfReady();
		}
	};
};

class boss_shadron : public CreatureScript
{
public:
	boss_shadron() : CreatureScript("boss_shadron") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_shadronAI (creature);
	}

	struct boss_shadronAI : public ScriptedAI
	{
		boss_shadronAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 SpellShadowbreathTimer;

		void Reset()
		{
			SpellShadowbreathTimer = 13000;
		}

		void EnterCombat(Unit* pWho) OVERRIDE
		{
			DoCast(me,SPELL_POWER_OF_SHADRON);
		}

		void UpdateAI(uint32 uiDiff) OVERRIDE
		{
			if (SpellShadowbreathTimer < uiDiff)
			{
				DoCast(me,SPELL_SHADOW_BREATH);
				SpellShadowbreathTimer = 26000;
			}
			else
				SpellShadowbreathTimer -= uiDiff;

			DoMeleeAttackIfReady();
		}
	};
};

class boss_vesperon : public CreatureScript
{
public:
	boss_vesperon() : CreatureScript("boss_vesperon") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_vesperonAI (creature);
	}

	struct boss_vesperonAI : public ScriptedAI
	{
		boss_vesperonAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 SpellShadowbreathTimer;

		void Reset()
		{
			SpellShadowbreathTimer = 13000;
		}

		void EnterCombat(Unit* pWho) OVERRIDE
		{
			DoCast(me,SPELL_POWER_OF_VESPERON);
		}

		void UpdateAI(uint32 uiDiff) OVERRIDE
		{
			if (SpellShadowbreathTimer < uiDiff)
			{
				DoCast(me,SPELL_SHADOW_BREATH);
				SpellShadowbreathTimer = 26000;
			}
			else
				SpellShadowbreathTimer -= uiDiff;

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_emoriss()
{
	new boss_emoriss;
	new boss_tenebron;
	new boss_shadron;
	new boss_vesperon;
}
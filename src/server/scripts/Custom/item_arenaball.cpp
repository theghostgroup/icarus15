/*
	   ArenaBall 
  Ball Handler ItemScript
For Icarus Core: Soul Thief
	  By MFerrill
*/

#include "ScriptMgr.h"
#include "AccountMgr.h"
#include "ArenaTeamMgr.h"
#include "Group.h"
#include "Language.h"
#include "MovementGenerator.h"
#include "ObjectAccessor.h"
#include "Opcodes.h"
#include "SpellAuras.h"
#include "Player.h"
#include "Chat.h"
#include "Unit.h"

enum ItemIDs
{
	ITEM_ARENA_BALL = 186620,
};
enum
{
	NPC_COMMENTATOR = 100023,
};

class ArenaBall_Item : public ItemScript
{
public:
	ArenaBall_Item() : ItemScript("ArenaBall_Item") { }

	bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const& /*targets*/) OVERRIDE
	{

		char msg[200];
		Unit* target = player->GetSelectedPlayer();
		Unit* commentator;

		if (player->GetName() == player->GetSelectedPlayer()->GetName())
		{
			sprintf(msg, "You cannot throw the ball to yourself!");
			player->MonsterTextEmote(msg, player);
			return true;
		}
		
		if (player->GetMapId() != 870 || player->GetAreaId() != 6823)
		{
			player->DestroyItemCount(ITEM_ARENA_BALL, 1, true);
			player->RemoveAurasDueToSpell(99998);
			sprintf(msg, "You cannot use the Arena Ball outside the Battle Ground!");
			player->MonsterTextEmote(msg, commentator);
			return true;
		}
		
		/* Handle by Spell.dbc (see spell id's 99999, and 99998)-- Outdated */
		
		/*Re-enabled No Target Cancellation of spell 
		Spell.dbc corrupted bt TC recent Merge
		Rebuild Spell.dbc*/
		
		if (!target) 
		{
				int randomfail = urand(0, 2);
				switch (randomfail) // Switching our randomizer
			{
				case 0: // 1
					sprintf(msg, "%s, tried to pass, but no one was open!!!", player->GetName());
					player->MonsterTextEmote(msg, commentator);
				case 1: // 2
					sprintf(msg, "%s, looks for an open player, but can't find one!!!", player->GetName());
					player->MonsterTextEmote(msg, commentator);
				case 2: // 3
					sprintf(msg, "%s, looks around frantically for a teammate to pass to, but finds no one open!!!", player->GetName());
					player->MonsterTextEmote(msg, commentator);
				break;
			}
			return true;
		}	

		
		if (target && player->getFaction() == player->GetSelectedPlayer()->getFaction())
		{
			player->RemoveAurasDueToSpell(99998);
			player->CastSpell(player, 99999, true);
			player->CastSpell(target, 99998, true);
			player->DestroyItemCount(ITEM_ARENA_BALL, 1, true);
			player->GetSelectedPlayer()->AddItem(ITEM_ARENA_BALL, 1);
			
			int randompass = urand(0, 5);
			switch (randompass) 
			{
			case 0: // 1
				sprintf(msg, "%s, just made a excellent pass to %s!!! What a great player!!!", player->GetName(), player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 1: // 2
				sprintf(msg, "%s, Made another great pass!!!", player->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 2: // 3
				sprintf(msg, "Oh now that pass to %s was just awesome!!!", player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 3: // 4
				sprintf(msg, "I can't believe %s just caught that pass, What a great move!!!", player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 4: // 5
				sprintf(msg, "%s, just caught a hell of a pass from %s!!! Talk about a winning combo!!!", player->GetSelectedUnit()->GetName(), player->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			}
			return true;
		}
		
		if (target && player->getFaction() != player->GetSelectedPlayer()->getFaction())
		{
			player->RemoveAurasDueToSpell(99998);
			player->CastSpell(player, 99999, true);
			player->CastSpell(target, 99998, true);
			player->DestroyItemCount(ITEM_ARENA_BALL, 1, true);
			player->GetSelectedPlayer()->AddItem(ITEM_ARENA_BALL, 1);
			
			int randomintercept = urand(0, 4);
			switch (randomintercept) // Switching our randomizer
			{
			case 0: // 1
				sprintf(msg, "%s, threw a bad pass and %s just intercepted the ball!", player->GetName(), player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 1: // 2
				sprintf(msg, "%s, made annother horrible pass!", player->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 2: // 3
				sprintf(msg, "Oh now that pass to %s was just awesome, too bad they're on the other team!", player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			case 3: // 4
				sprintf(msg, "I can't believe %s just caught that pass, What a great move!!!", player->GetSelectedUnit()->GetName());
				player->MonsterTextEmote(msg, commentator);
				break;
			}
			return true;
		}
		return true;
	}
};

void AddSC_ArenaBall_Item()
{
	new ArenaBall_Item();
}
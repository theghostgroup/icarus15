/*
Icarus Gaming Inc
		&
Umbra Gaming Inc
   By Nobody 
	  Aka
Matthew Ferrill
*/

#include "ScriptPCH.h"
#define TOKEN_ID   160000   // Replace 160000 to YOUR_TOKEN_ID

class Level_NPC : public CreatureScript
{
public:
	Level_NPC() : CreatureScript("Level_NPC") {}

	bool OnGossipHello(Player* pPlayer, Creature* _creature)
	{
		pPlayer->ADD_GOSSIP_ITEM(7, "Welcome to the level NPC!", GOSSIP_SENDER_MAIN, 0);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 10 (1 Token)", GOSSIP_SENDER_MAIN, 1);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 20 (2 Tokens)", GOSSIP_SENDER_MAIN, 2);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 30 (3 Tokens)", GOSSIP_SENDER_MAIN, 3);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 40 (4 Tokens)", GOSSIP_SENDER_MAIN, 4);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 50 (5 Tokens)", GOSSIP_SENDER_MAIN, 5);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 60 (6 Tokens)", GOSSIP_SENDER_MAIN, 6);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 70 (7 Tokens)", GOSSIP_SENDER_MAIN, 7);
		pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 80 (8 Tokens)", GOSSIP_SENDER_MAIN, 8);

		pPlayer->PlayerTalkClass->SendGossipMenu(907, _creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* pPlayer, Creature* _creature, uint32 uiSender, uint32 uiAction)
	{
		pPlayer->PlayerTalkClass->ClearMenus();
		if(uiAction != 0)
			if (pPlayer->HasItemCount(TOKEN_ID, uiAction, true))
			{
				//pPlayer->SetLevel(uiAction*10); // changed to setlevel, give level gives X amount of levels I think.
				pPlayer->GiveLevel(uiAction*10);
				pPlayer->DestroyItemCount(TOKEN_ID, uiAction, true);
				pPlayer->GetSession()->SendAreaTriggerMessage("You are now Level %u!", uiAction*10);
				pPlayer->PlayerTalkClass->SendCloseGossip();
				return true;
			}
			else
				pPlayer->GetSession()->SendNotification("You need more tokens for that.");
		OnGossipHello(pPlayer, _creature);
		return true;
	}
};
void AddSC_Level_NPC()
{
	new Level_NPC();
}
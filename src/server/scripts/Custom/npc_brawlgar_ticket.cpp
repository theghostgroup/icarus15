/*
Copyright 2014 to 2018 Icarus Gaming Inc.
By MFerrill
For Soul Thief Server
*/

#include "ScriptPCH.h"
#define NORMAL_TOKEN_ID_ONE 	676767   
#define NORMAL_TOKEN_ID_TWO 	676768
#define VIP_TOKEN_ID   			676769

class Brawlgar_Ticket_Npc : public CreatureScript
{
public:
	Brawlgar_Ticket_Npc() : CreatureScript("Brawlgar_Ticket_Npc") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(7, "Welcome to the Arena!", GOSSIP_SENDER_MAIN, 0);
		player->ADD_GOSSIP_ITEM(10, "Send me to my seat!", GOSSIP_SENDER_MAIN, 1);
		//player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
		player->PlayerTalkClass->SendGossipMenu(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
	{
		player->PlayerTalkClass->ClearMenus();
		if (uiAction != 0)
			
			if (player->HasItemCount(NORMAL_TOKEN_ID_ONE, uiAction, true))
			{
				player->DestroyItemCount(NORMAL_TOKEN_ID_ONE, uiAction, true);
				player->PlayerTalkClass->SendCloseGossip();
				player->TeleportTo(1043, 2035.317f, -4792.487f, 96.1f, 1.714f);
				return true;
			}
			
			if (player->HasItemCount(NORMAL_TOKEN_ID_TWO, uiAction, true))
			{
				player->DestroyItemCount(NORMAL_TOKEN_ID_TWO, uiAction, true);
				player->PlayerTalkClass->SendCloseGossip();
				player->TeleportTo(1043, 2030.343f, -4712.862f, 96.1f, 4.838f);
				return true;
			}
			
			if (player->HasItemCount(VIP_TOKEN_ID, uiAction, true))
			{
				player->DestroyItemCount(VIP_TOKEN_ID, uiAction, true);
				player->PlayerTalkClass->SendCloseGossip();
				player->TeleportTo(1043, 1993.656f, -4754.497f, 93.1f, 3.147f);
				return true;
			}
						
			else
				player->GetSession()->SendNotification("You need a ticket First!");
				OnGossipHello(player, creature);
				return true;
		
	}
};
void AddSC_Brawlgar_Ticket_Npc()
{
	new Brawlgar_Ticket_Npc();
}
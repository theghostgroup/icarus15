/*
Icarus Gaming Custom Boss
© Copyright Icarus Gaming Inc 2013. All rights reserved.
By Josh Carter
*/

/*
ScriptData
SDCategory: Firelands
SDName: Boss Ragemaul
SD%Complete: 90%
SDComment: Needs in Game Test
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"
#include "instance_firelands.h"

enum SpawnIDs 
{
NPC_RAGEMAUL_BOSS						= 300105,
NPC_RAGEMAUL_TRASH						= 300106,
};

enum YellIds
{
SAY_BOSS_DEATH		= 0,
SAY_RUNAWAY			= 1,
SAY_PLAYER_DEATH	= 2,
SAY_AGGRO			= 3,
};

enum RagemaulSpells
{
SPELL_BERSERK     				        = 41924,
SPELL_DRAGON_MASSIVE_FLAMESTRIKE 		= 26558,
SPELL_CHARRED_EARTH						= 30129,
SPELL_THUNDER_STORM						= 59154,
SPELL_PERIODIC_VIOLENT_STORM			= 53229,
SPELL_PERIODIC_SHADOWSTORM				= 2148,
SPELL_METEOR_STRIKE						= 75879, 
SPELL_METEOR_FISTS						= 67331,
SPELL_RAIN_OF_CHAOS						= 71965,

};

class boss_ragemaul : public CreatureScript
{
public:
	boss_ragemaul() : CreatureScript("boss_ragemaul") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_ragemaulAI (creature);
	}

	struct boss_ragemaulAI : public ScriptedAI
	{
		boss_ragemaulAI(Creature* pCreature) : ScriptedAI(pCreature) {
			x = me->GetPositionX();
			y = me->GetPositionY();
			z = me->GetPositionZ();
		}
		float x,y,z;
		char msg[200];
		bool Phase1;
		bool Phase2;
		bool Phase3;
		bool ShadowStorm;
		bool ViolentStorm;
		bool Enraged;
		uint32 ThunderTimer;
		uint32 FireAttackTimer;
		uint32 MetorFistTimer;
		uint32 MeteorStrikeTimer;
		uint32 EnragedTimer;
		uint32 ReinofchaosTimer;
		
		void Reset()
		{
		me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
		me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, UNIT_BYTE1_FLAG_ALWAYS_STAND);
		Phase1 = false;
		Phase1 = false;
		Phase1 = false;
		ShadowStorm	= false;
		ViolentStorm = false;
		Enraged = false;
		EnragedTimer = 1200000; //20 min
		ThunderTimer = 60000; // 1 min
		FireAttackTimer = 45000; // 45 Seconds
		MetorFistTimer = 35000; // 35 seconds
		MeteorStrikeTimer = 20000; //20 seconds
		ReinofchaosTimer = 40000;
		}
		
		void JustDied(Unit* /*killer*/) OVERRIDE
		{
		Talk(SAY_BOSS_DEATH);
		}
		
		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
		me->MonsterYell("Debug mssg: Combat True!!!", LANG_UNIVERSAL, me);
		Talk(SAY_AGGRO);
		ShadowStorm	= true;
		Phase1 = true;
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
		Talk(SAY_PLAYER_DEATH);
		}
		
		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;	
			
			if (EnragedTimer <=diff && !Enraged)
				{
				DoCast(me, SPELL_BERSERK, true);
				me->MonsterYell("Burn little rodents!!!", LANG_UNIVERSAL, me);
				Enraged = true;
				}
				
			if(HealthBelowPct(100) && ShadowStorm) 
				{
				me->AddAura(SPELL_PERIODIC_SHADOWSTORM, me);
				ShadowStorm = false;
				}	
			
			if(HealthBelowPct(100) && ViolentStorm) 
				{
				DoCast(me, SPELL_PERIODIC_VIOLENT_STORM, true);
				sprintf(msg,"A violent storm fueled by %s's pure hatred erupts!",me->GetName().c_str());
				ViolentStorm = false;
				}
			
			if (HealthBelowPct(66))
				{
				Phase1 = false;
				Phase2 = true;
				Phase3 = false;
				}

			if (HealthBelowPct(33))
				{
				Phase1 = false;
				Phase2 = false;
				Phase3 = true;
				}
			
				if (Phase1)
				{
					if (FireAttackTimer <= diff)
					{
						if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						sprintf(msg,"%s is Debugged and %s is targeted for Dragon Flame!",me->GetName().c_str(),Target->GetName().c_str());
						DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE);
						DoCast(Target, SPELL_CHARRED_EARTH);
						Talk(SAY_RUNAWAY);
						FireAttackTimer = urand(25000, 35000);
						}
					} else FireAttackTimer -= diff;
					
					if (ThunderTimer <= diff)
					{
					DoCast(me, SPELL_THUNDER_STORM, true); // May need to change to: DoCast(Target, SPELL_THUNDER_STORM, true);
					me->MonsterYell("ARG!! Get Away From Me NOW!!!", LANG_UNIVERSAL, me);
					ThunderTimer = urand(45000, 65000);
					} else ThunderTimer -= diff;
				}
						
				if (Phase2)
				{
					if (FireAttackTimer <= diff)
					{
						if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE,true);
						DoCast(Target, SPELL_CHARRED_EARTH, true);
						Talk(SAY_RUNAWAY);
						FireAttackTimer = urand(25000, 35000);
						}
					} else FireAttackTimer -= diff;
					
					if (ThunderTimer <= diff)
					{
					DoCast(me, SPELL_THUNDER_STORM, true); // May need to change to: DoCast(Target, SPELL_THUNDER_STORM, true);
					me->MonsterYell("ARG!! Get Away From Me NOW!!!", LANG_UNIVERSAL, me);
					sprintf(msg,"%s is Debugged and this sprintf message is setup correctly!",me->GetName().c_str());
					ThunderTimer = urand(45000, 65000);
					} else ThunderTimer -= diff;
				
					if (ReinofchaosTimer <= diff)
					{
					me->MonsterYell("DEBUG MESSAGE One TRUE!!!", LANG_UNIVERSAL, me);
					DoCastVictim(SPELL_CHARRED_EARTH, true);
					DoCastVictim(SPELL_RAIN_OF_CHAOS, true);
					ReinofchaosTimer = urand(10000, 65000);
					}else ReinofchaosTimer -= diff;
				}
						
				if (Phase3)
				{
					if (FireAttackTimer <= diff)
						{
							if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
							{
							DoCast(Target, SPELL_DRAGON_MASSIVE_FLAMESTRIKE);
							DoCast(Target, SPELL_CHARRED_EARTH);
							Talk(SAY_RUNAWAY);
							FireAttackTimer = urand(25000, 35000);
							}
						} else FireAttackTimer -= diff;
					
					if (ThunderTimer <= diff)
						{
						DoCast(me, SPELL_THUNDER_STORM, true); // May need to change to: DoCast(Target, SPELL_THUNDER_STORM, true);
						me->MonsterYell("ARG!! Get Away From Me NOW!!!", LANG_UNIVERSAL, me);
						ThunderTimer = urand(45000, 65000);
						} else ThunderTimer -= diff;
				
					if (ReinofchaosTimer <= diff)
						{
						me->MonsterYell("DEBUG MESSAGE One TRUE!!!", LANG_UNIVERSAL, me);
						DoCastVictim(SPELL_CHARRED_EARTH, true);
						DoCastVictim(SPELL_RAIN_OF_CHAOS, true);
						ReinofchaosTimer = urand(10000, 65000);
						} else ReinofchaosTimer -= diff;
					
					if (MetorFistTimer <= diff)
						{
						me->MonsterYell("DEBUG MESSAGE Two TRUE!!!", LANG_UNIVERSAL, me);
						DoCastVictim(SPELL_CHARRED_EARTH, true);
						DoCastVictim(SPELL_RAIN_OF_CHAOS, true);
						MetorFistTimer = urand(10000, 65000);
						} else MetorFistTimer -= diff;
					
					if (MeteorStrikeTimer <= diff)
					{
						me->MonsterYell("DEBUG MESSAGE Three TRUE!!!", LANG_UNIVERSAL, me);
						DoCastVictim(SPELL_CHARRED_EARTH, true);
						DoCastVictim(SPELL_RAIN_OF_CHAOS, true);
						MeteorStrikeTimer = urand(10000, 65000);
					} else MeteorStrikeTimer -= diff;
				}
			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_ragemaul()
{
	new boss_ragemaul;
}
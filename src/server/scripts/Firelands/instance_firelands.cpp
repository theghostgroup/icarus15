/**************************************************\
* Instance Firelands: (C) Icarus-Gaming Inc 2013  *
*    Written By Josh Carter @ Icarus-Gaming Inc   *
*                TC2_255_BC                       *
/**************************************************\

/* ScriptData
SDName: Instance Firelands
SDComment: Needs a lot of work
SDData:10%
SDCategory: Firelands
EndScriptData */

#include "ScriptPCH.h"
#include "Player.h"
#include "instance_firelands.h"

#define MAX_ENCOUNTER  1

class instance_firelands : public InstanceMapScript
{
public:
	instance_firelands() : InstanceMapScript("instance_firelands", 42) { } 

	InstanceScript* GetInstanceScript(InstanceMap* pMap) const
	{
		return new instance_firelands_InstanceMapScript(pMap);
	}

	struct instance_firelands_InstanceMapScript : public InstanceScript
	{
		instance_firelands_InstanceMapScript(Map* pMap) : InstanceScript(pMap) {}

		uint32 m_auiEncounter[MAX_ENCOUNTER];
		uint32 TeamInInstance;

		uint16 uiMovementDone;
		uint8 uiAgroDone;
		uint8 uiAggroDone;
		std::string str_data;
		bool bDone;

		void Initialize() // Need to Find out more Data about How we should get it to work.
		{
			uiMovementDone = 0;
			bDone = false;
			memset(&m_auiEncounter, 0, sizeof(m_auiEncounter));
		}

		bool IsEncounterInProgress() const
		{
			for (uint8 i = 0; i < MAX_ENCOUNTER; ++i)
			{
				if (m_auiEncounter[i] == IN_PROGRESS)
					return true;
			}

			return false;
		}
		void HandleSpellOnPlayersInInstanceFirelands(Unit* caller, uint32 spellId) // Lets check and gic right spell to players in the zone
		{
			if (spellId <= 0 || !caller)
				return;

			Map* map = caller->GetMap();
			if (map && map->IsDungeon())
			{
				Map::PlayerList const &PlayerList = map->GetPlayers();

				if (PlayerList.isEmpty())
					return;

				for (Map::PlayerList::const_iterator i = PlayerList.begin(); i != PlayerList.end(); ++i)
					if (i->GetSource() && i->GetSource()->IsAlive() && !i->GetSource()->IsGameMaster())
						caller->CastSpell(i->GetSource(), spellId);
			}
		}
	};
};

void AddSC_instance_firelands()
{
	new instance_firelands();
}
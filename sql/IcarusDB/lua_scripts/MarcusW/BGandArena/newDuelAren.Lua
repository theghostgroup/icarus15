nDA = {}
nDA.plrs ={}
nDA.name = "Brawl'Gar"
nDA.areaId = 6298
nDA.playername_1 = nil-- Hacky
nDA.playername_2 = nil-- Hacky
nDA.battlemaster = nil
function nDA.check_start_battle(isOccu)
	if isOccu == false then
		local switchSpawnSpot = 1
		for k, v in pairs(nDA.plrs) do
			if switchSpawnSpot == 1 then
				nDA.playername_1 = v:GetName()
				v:ResetAllCooldowns()
				v:Teleport(1043, 2031.319214, -4765.165527, 88.372620, 1.605245) -- raised zCoord
				switchSpawnSpot = 2	
			else
				nDA.playername_2 = v:GetName()
				v:ResetAllCooldowns()
				v:Teleport(1043, 2030.890259, -4742.142090, 88.372620, 4.756801) -- raised zCoord
				switchSpawnSpot = 1
			end
		end
		nDA.battlemaster:SendUnitYell("Get ready for a show!", 0)
		nDA.battlemaster:SendUnitYell("".. nDA.playername_1 .." will battle ".. nDA.playername_2 .."!", 0)
		nDA.battlemaster:SendUnitYell("Who wants to see blood?", 0)
	else -- Because CreateLuaEvent is not getting called this does not happen -- This is the area check / if plr is in que and is in another area check.
	---Everything from here should be rewritten and optimized-----
		for k, v in pairs(nDA.plrs) do
			if v then
				if v:GetAreaId() ~= nDA.areaId then
					for k,v in pairs(nDA.plrs) do
						v:ResurrectPlayer(100)
						v:ResetAllCooldowns()
						v:Teleport(1043, 2070.353516, -4752.817871, 88.775620, 3.156116)
						if v:GetName() == nDA.playername_2 then
							CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_1 .."'")
						else
							CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_2 .."'")
						end
					end
				end
				nDA.plrs = {}
			end
		end
	end
end	
function nDA.on_plr_changeMap(event, player, newZone, newArea)
	if newArea ~= nDA.areaId then
		for k, v in pairs(nDA.plrs) do
			if v then
				if v:GetName() == player:GetName() then
					for k,v in pairs(nDA.plrs) do
						if v:GetName() ~= player:GetName() then
							v:ResurrectPlayer(100)
							v:ResetAllCooldowns()
							v:Teleport(1043, 2070.353516, -4752.817871, 88.775620, 3.156116)
						elseif v:GetName() == nDA.playername_2 then
							print(v:GetName(), nDA.playername_2)
							CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_2 .."'")
						elseif v:GetName() == nDA.playername_1 then
							print(v:GetName(), nDA.playername_1)
							CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_1 .."'")
						end
					end
					nDA.plrs = {}
				end
			end
		end
	end
end
RegisterPlayerEvent(27, nDA.on_plr_changeMap)

function nDA.on_plr_logout(event, player)
	if player:GetAreaId() == nDA.areaId then
		for k, v in pairs(nDA.plrs) do
			if v:GetName() ~= player:GetName() then
				v:ResurrectPlayer(100)
				v:ResetAllCooldowns()
				v:Teleport(1043, 2070.353516, -4752.817871, 88.775620, 3.156116)
			elseif v:GetName() == nDA.playername_2 then
				print(v:GetName(), nDA.playername_2)
				CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_2 .."'")
			elseif v:GetName() == nDA.playername_1 then
				print(v:GetName(), nDA.playername_1)
				CharDBQuery("UPDATE characters SET position_x='2070.353516', position_y='-4752.817871', position_z='88.775620', map='1043' WHERE name='".. nDA.playername_1 .."'")
			end
		end
		nDA.plrs = {}
	end
end
RegisterPlayerEvent(4, nDA.on_plr_logout)
----To here---------------------------------------------

function nDA.onPlayerKilledPush(killer, killed)
	killer:SetHealth(killer:GetMaxHealth())
	killer:ResetAllCooldowns()
	killer:Teleport(1043, 2070.353516, -4752.817871, 88.775620, 3.156116)
	
	killed:ResurrectPlayer(100)
	killed:ResetAllCooldowns()
	killed:Teleport(1043, 2070.353516, -4752.817871, 88.775620, 3.156116)
	killed:SetHealth(killer:GetMaxHealth())
	nDA.plrs = {}
	nDA.battlemaster:SendUnitYell("The heroic ".. killer:GetName() .." has defeated ".. killed:GetName() .."", 0)
end

function nDA.onPlayerKilled(event, killer, killed)
	if killer:GetAreaId() == nDA.areaId then
		nDA.onPlayerKilledPush(killer, killed)
	end
end
function nDA.onPlayerKilledByCrea(event, killer, killed)
	if killer:GetAreaId() == nDA.areaId then
		if killer:GetOwnerGUID() then
			killer = killer:GetOwner()
		end
		nDA.onPlayerKilledPush(killer, killed)
	end
end
RegisterPlayerEvent(6, nDA.onPlayerKilled)
RegisterPlayerEvent(8, nDA.onPlayerKilledByCrea)


function nDA.isPlrInQue(player)
	for k,v in pairs(nDA.plrs) do
		if v then
			if(player:GetName() == v:GetName()) then
				return true
			end
		end
	end
end

function nDA.Battlemaster(event, player, unit)
	local opponentName = ""
	--Instead of declaring the npc as battlemaster upon spawn/respawn we do it in this hook.
		if nDA.battlemaster == nil then
			nDA.battlemaster = unit
		end
	----------------------------------------------------------------------------------------
	if #nDA.plrs < 2 then
		if (nDA.isPlrInQue(player) ~= true) then
			if #nDA.plrs == 0 then
				player:GossipMenuAddItem(9, 'Join Arena', 0, 1)
			else
				for k,v in pairs(nDA.plrs) do
					opponentName = v:GetName()
				end
				player:GossipMenuAddItem(9, 'Battle "'.. opponentName ..'" ', 0, 1)
			end
		elseif nDA.isPlrInQue(player) then
			player:GossipMenuAddItem(2, 'Leave Arena', 0, 2)
		end
		player:GossipMenuAddItem(0, 'Never mind...', 0, 4)
	else
		player:GossipMenuAddItem(0, 'The arena is currently occupied.\nCheck back later or spectate.', 0, 3)
	end
	player:GossipSendMenu(1, unit)
end

function nDA.Battlemaster_select(event, player, unit, sender, intid, code)
	if intid == 1 then
		table.insert(nDA.plrs, player)
		player:SendAreaTriggerMessage("|CFFFF0303[".. nDA.name .."] |cffffff00You are now in the que for ".. nDA.name .."")
		if #nDA.plrs == 2 then
			nDA.check_start_battle(false)
		else
			nDA.battlemaster:SendUnitYell("".. player:GetName() .." has joined the arena. How valiant!", 0)
			player:SendBroadcastMessage("|CFFFF0303[".. nDA.name .."] |cffffff00Waiting for one more player...")
		end
		player:GossipComplete()
	elseif intid == 2 then
		for k,v in pairs(nDA.plrs) do
			if(v:GetName() == player:GetName()) then
				player:SendBroadcastMessage("|CFFFF0303[".. nDA.name .."] |cffffff00You are no longer in que for: ".. nDA.name .."")
				player:SendAreaTriggerMessage("|CFFFF0303[".. nDA.name .."] |cffffff00You are no longer in que for: ".. nDA.name .."")
				nDA.plrs = {}
				nDA.battlemaster:SendUnitYell("".. player:GetName() .." has left the arena... what a coward!", 0)
			else -- Should never happen, as players can't choose to leave que when they are not in que.
				playeR:SendNotification("Couldn't find you in the que list, please contact an administrator.")
			end
		end
		player:GossipComplete()
	else
		player:GossipComplete()
	end
end
RegisterCreatureGossipEvent(80011, 1, nDA.Battlemaster)
RegisterCreatureGossipEvent(80011, 2, nDA.Battlemaster_select)
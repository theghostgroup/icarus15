hB = {}
hB.AreaId = 6823
hB.reqPlayers = 2
hB.plrs = {}
hB.storage = {}
hB.Started = false
hB.commentator = nil
hB.maxScore = 2
hB.name = "Arena Ball"
hB.Score = {
[1] = 0,
[2] = 0
}
hB.Runner = nil
hB.turnInNpc = 130000
hB.chestId = 302337
hB.chest_spawnPoint = {870, -232.076, -5358.201, 123.715, 1.306}
hB.chest = nil
hB.Morgalis = 15055841
hB.Turn_In_Range = 1

-------------Commentator sentences-----------------
--[[hB.cmm_sentences = {
	{---Id_1---
		{"has turned in the ball!"},
		{"has scored, what a great play"},
		{"has delivered the ball!"}
	},
	{---Id_2---
		{"has taken the ball from the chest!"},
		{"has picked up the ball!"},
		{"has taken the ball!"}
	},
	{---Id_3---
		{"has stolen the ball from"},
		{"has snatched the ball from"}
	},
	{---Id_4---
		{"Morganis has swallowed the ball! Oh and "},
		{"might be missing a hand..."},
		{"Ups, the ball is gone!"},
		{"Ball location: Only Morganis knows!"},
		{"has been eaten by Morgalis! Ouch, that looked painful..."}
	},
	{---Id_5---
		{"has pulverized"},
		{"has smashed"},
		{"has annihilated"}
	},
	{---Id_6
		{"has caught the ball!"},
		{"made a beautiful catch!"},
		{"has been thrown the ball!"}
	},
	{---Id_7---
		{"missed the target!"},
		{"how could you miss that?"},
		{"has made a terrible throw!"},
		{"should quit arena balling!"},
		{"seems to be blind!"}
	},
	{---Id_8---
		{"has intercepted the ball!"},
		{"stole the ball mid air!"}
	}
}
function hB.return_sent(sentId)
	local sent = math.random(1, #hB.cmm_sentences[sentId])
	return hB.cmm_sentences[sentId][sent][1]
	
end--]]
-------------------The game:-----------------------
function hB.isRunner(player)
	if hB.Runner == player:GetName():lower() then
		return true
	else
		return false
	end
end
function hB.SetRunner(player)
	player:SetScale(1.3)
	player:CastSpell(player, 99998, true) -- Add Mark to Runner (True = Instant)
	player:AddItem(186620, 1) -- Add (1) Arena Ball Item to Runner
	hB.Runner = player:GetName():lower()
end

function hB.checkQue()
	if #hB.plrs == hB.reqPlayers then
		hB.Started = true
		hB.Runner = nil
		hB.storage = {}
		local team = 1
		for k, v in pairs(hB.plrs) do
			hB.storage[tostring(v)] = {v:GetMapId(), v:GetX(), v:GetY(), v:GetZ(), v:GetO()}
			if team == 1 then
				v:SetFaction(team)			
				v:ResetAllCooldowns()
				v:SetHealth(v:GetMaxHealth())
				v:Teleport(870, -183.252594, -5128.474609, 124.715630, 4.53484)
				team = team + 1
			else
				v:SetFaction(team)
				v:ResetAllCooldowns()
				v:SetHealth(v:GetMaxHealth())			
				v:Teleport(870, -416.778931, -5373.054688, 130.962112, 5.618129)
				team = 1
			end
		end
		hB.commentator = PerformIngameSpawn(1, 100023, 870, 0, -127.649, -5385.609, 150.104, 2.95)
		print("Commentator is Spawned... Attempting to spawn Chest")
		hB.chest = PerformIngameSpawn(2, hB.chestId, hB.chest_spawnPoint[1], 0, hB.chest_spawnPoint[2], hB.chest_spawnPoint[3], hB.chest_spawnPoint[4], hB.chest_spawnPoint[5])
		print(hB.chest, 'Chest spawned succesfully')
	end
end

function hB.onTurnIn(event, creature, player)
	if event == 5 then
		creature:SetVisible(false)
		creature:SetFaction(35)
	end
	if (player:GetName() and creature:IsWithinDistInMap(player, hB.Turn_In_Range) and hB.isRunner(player)) then
		local team = player:GetFaction()
		hB.chest = PerformIngameSpawn(2, hB.chestId, hB.chest_spawnPoint[1], 0, hB.chest_spawnPoint[2], hB.chest_spawnPoint[3], hB.chest_spawnPoint[4], hB.chest_spawnPoint[5])	
		hB.Runner = nil
		player:SetScale(1)
		player:RemoveAura(99998) -- Remove Runner's Marker
		player:RemoveItem(186620, 1) -- Remove the Arena Ball	
		hB.commentator:MoveClear()
		hB.commentator:MoveTo(1, hB.chest_spawnPoint[2] + 2, hB.chest_spawnPoint[3] + 2, hB.chest_spawnPoint[4] + 5)
		hB.Score[team] = hB.Score[team] + 1
		hB.commentator:SendUnitYell("".. player:GetName() .." ".. hB.return_sent(1) .."", 0)
		if hB.Score[team] == hB.maxScore then
			hB.Started = false
			for k,v in pairs (hB.plrs) do
				v:RemoveItem(186620, 1) -- Remove the Arena Ball	
				v:ResurrectPlayer(100)
				v:ResetAllCooldowns()
				local race = v:GetRace()
				if race == 1 or race == 3 or race == 4 or race == 7 or race == 11 then
					v:SetFaction(1)
				else
					v:SetFaction(2)
				end
				v:SendBroadcastMessage("Team ".. team .." has won!")
				v:Teleport(hB.storage[tostring(v)][1], hB.storage[tostring(v)][2], hB.storage[tostring(v)][3], hB.storage[tostring(v)][4], hB.storage[tostring(v)][5])
			end
			hB.plrs.storage = {}
			hB.plrs = {}
		end
	end
end
RegisterCreatureEvent(hB.turnInNpc, 5, hB.onTurnIn)
RegisterCreatureEvent(hB.turnInNpc, 27, hB.onTurnIn)

function hB.chestPickUp(event, player, go)
    if	hB.Runner == nil	then
		hB.SetRunner(player)
		hB.chest:Despawn()
		go:Despawn()
		print(hB.return_sent(2))
		hB.commentator:SendUnitYell("".. player:GetName() .." ".. hB.return_sent(2) .."", 0)
		hB.commentator:MoveFollow(player, 6, 90)
	end
end
RegisterGameObjectGossipEvent(hB.chestId, 1, hB.chestPickUp)

function hB.onPlayerKilled(event, killer, killed)
	if killer:GetAreaId() == hB.AreaId then
		if killer:GetName() == killed:GetName() then
			if isRunner(killed) then
				hB.chest = PerformIngameSpawn(2, hB.chestId, hB.chest_spawnPoint[1], 0, hB.chest_spawnPoint[2], hB.chest_spawnPoint[3], hB.chest_spawnPoint[4], hB.chest_spawnPoint[5])
				hB.commentator:MoveClear()
			end
			hB.commentator:SendUnitYell("".. killed:GetName() .." has committed suicide!", 0)
		elseif killer:GetGUIDLow() == hB.Morgalis then
			if isRunner(killed) then
				hB.chest = PerformIngameSpawn(2, hB.chestId, hB.chest_spawnPoint[1], 0, hB.chest_spawnPoint[2], hB.chest_spawnPoint[3], hB.chest_spawnPoint[4], hB.chest_spawnPoint[5])
				hB.commentator:MoveClear()
				hB.Runner = nil
			end
			hB.commentator:SendUnitYell("".. player:GetName() .." ".. hB.return_sent(4) .."", 0)
		else
			if (killer:GetName() == nil and killer:GetOwnerGUID()) then
				killer = killer:GetOwner()
			end
			if hB.isRunner(killed) then
				
				killed:SetScale(1)
				killed:RemoveAura(99998) -- Remove Mark From Killed
				killed:RemoveItem(186620, 1) -- Remove the Arena Ball
				hB.SetRunner(killer)
				
				hB.commentator:MoveClear()
				hB.commentator:MoveFollow(killer, 6, 90)
				hB.commentator:SendUnitYell("".. killer:GetName() .." ".. hB.return_sent(3) .."  ".. killed:GetName() .."", 0)
			else
				hB.commentator:SendUnitYell("".. killer:GetName() .." ".. hB.return_sent(5) .." ".. killed:GetName() .."", 0)
			end
		end
	end
end

function hB.onPlayerKilledByCrea(event, killer, killed)
	if killer:GetAreaId() == hB.AreaId then
		if killer:GetOwnerGUID() then
			killer = killer:GetOwner()
		end
		hB.onPlayerKilled(nil, killer, killed)
	end
end
RegisterPlayerEvent(6, hB.onPlayerKilled)
RegisterPlayerEvent(8, hB.onPlayerKilled)
RegisterPlayerEvent(8, hB.onPlayerKilledByCrea)
								
								--[[ Handled By ItemScript ]]--
								
--[[function hB.onSpellCast(event, player, spell, skipCheck)
    if (spell:GetEntry() == 99999) then
		if player:GetAreaId() == hB.AreaId then
			if hB.isRunner(player) then
				if player:GetSelection():IsWithinLoS(player:GetX(), player:GetY(), player:GetZ()) then
				local LoS = player:GetSelection():IsWithinLoS(player:GetX(), player:GetY(), player:GetZ())
					print('IsWithinLoS', LoS)
					hB.SetRunner(player:GetSelection())
					player:SetScale(1)
					hB.commentator:MoveClear()
					hB.commentator:MoveFollow(player:GetSelection(), 6, 90)
					hB.commentator:SendUnitYell("".. player:GetSelection():GetName() .." ".. hB.return_sent(6) .."", 0)
				else
					local Enemy_Who_Catch = player:GetUnfriendlyUnitsInRange(10)
					local dist = 10
					for k,v in pairs(Enemy_Who_Catch) do
						if v then
							if player:GetDistance(v) < dist then
								dist = player:GetDistance(v)
								Enemy_Who_Catch = v
							end
						else
							Enemy_Who_Catch = nil
						end
					end
					if ( Enemy_Who_Catch ~= nil and Enemy_Who_Catch:IsWithInLoS(player:GetX(), player:GetY(), player:GetZ()) )	then
						hB.SetRunner(Enemy_Who_Catch)
						player:SetScale(1)
						hB.commentator:MoveClear()
						hB.commentator:MoveFollow(Enemy_Who_Catch, 6, 90)
						hB.commentator:SendUnitYell("".. Enemy_Who_Catch:GetName() .." ".. hB.return_sent(8) .."", 0)
					else
						hB.chest = PerformIngameSpawn(2, hB.chestId, hB.chest_spawnPoint[1], 0, hB.chest_spawnPoint[2], hB.chest_spawnPoint[3], hB.chest_spawnPoint[4], hB.chest_spawnPoint[5])
						hB.commentator:MoveClear()
						hB.Runner = nil
						hB.commentator:SendUnitYell("".. Player:GetName() .." ".. hB.return_sent(7) .."", 0)
					end
				end
			else
				spell:Cancel()
				player:SendNotification('You do not have the ball.')
			end
		else
			spell:Cancel()
			player:SendNotification('You are not in an Arena Ball game.')
		end
	end
end
RegisterPlayerEvent(5, hB.onSpellCast) --]]

function hB.openMenu(event, player, unit)
	local plrIsInQue = false
	for k,v in pairs(hB.plrs) do
		if v then
			if(player:GetName() == v:GetName()) then
				plrIsInQue = true
			end
		end
	end
	if (#hB.plrs <= 1 and not hB.Started) then
		player:GossipMenuAddItem(5, 'Players currently in que: '.. #hB.plrs ..' / '.. hB.reqPlayers ..'', 0, 0)
	elseif hB.Started then
		if hB.Score[1] > hB.Score[2] then
			player:GossipMenuAddItem(5, 'Arena Ball has been started.\nTeam 1 is leading with a score of: '.. hB.Score[1] ..'', 0, 0)
		elseif hB.Score[1] < hB.Score[2] then
			player:GossipMenuAddItem(5, 'Arena Ball has been started.\nTeam 2 is leading with a score of: '.. hB.Score[2] ..'', 0, 0)
		else
			player:GossipMenuAddItem(5, 'Arena Ball has been started.\nTeams have an equal score of: '.. hB.Score[1] ..'', 0, 0)
		end
	end
	if (not plrIsInQue and hB.Started == false) then
		player:GossipMenuAddItem(9, 'Join ArenaBall', 0, 1)
	end
	if plrIsInQue then
		player:GossipMenuAddItem(2, 'Leave ArenaBall', 0, 2)
	end
	player:GossipMenuAddItem(0, 'Nevermind', 0, 3)
	player:GossipSendMenu(100, unit, 123001)
end

function hB.menu_Select(event, player, unit, sender, intid, code)
	if intid == 1 then
		table.insert(hB.plrs, player)
		hB.checkQue()
		player:GossipComplete()
	elseif intid == 2 then
		for k,v in pairs(hB.plrs) do
			if(v:GetName() == player:GetName()) then
				player:SendBroadcastMessage("|CFFFF0303[Arena Ball] |cffffff00You are no longer in que for: ".. hB.name .."")
				player:SendAreaTriggerMessage("|CFFFF0303[Arena Ball] |cffffff00You are no longer in que for: ".. hB.name .."")
				v:Teleport(hB.storage[tostring(v)][1], hB.storage[tostring(v)][2], hB.storage[tostring(v)][3], hB.storage[tostring(v)][4], hB.storage[tostring(v)][5])
				hB.storage[tostring(v)] = nil
				hB.plrs[v] = nil
				hB.plrs[k] = nil
				table.remove(hB.plrs, k)
			end
		end
		player:GossipComplete()
	elseif intid == 3 then
		player:GossipComplete()
	end
end
RegisterPlayerGossipEvent(123001, 2, hB.menu_Select)

function hB.menuChat(event, player, msg, Type, lang)
	msg = msg:lower()
	if(msg == "#arenaball menu" or msg == "#ball") then
		player:GossipClearMenu()
		hB.openMenu(1, player, player)
		return false
	end
end
RegisterPlayerEvent(18, hB.menuChat)
RegisterPlayerEvent(19, hB.menuChat)
RegisterPlayerEvent(20, hB.menuChat)
RegisterPlayerEvent(21, hB.menuChat)

--[[ 	MF:		--
	Added Ball Handling in C++
	Added Script to Lua folder for Reference
		
		It handles Passing and Messaging
		
		Pass to Self Fail Message
		Pass to Non-Player Fail Message
		Using outside arena Fail Message & Destroy Ball
		Pass to Team-mate item handling, aura handling, and messaging
		Pass to Non-Team-mate item handling, aura handling, and messaging
		
		It does Not handle Player->Killer receive ball, and Aura
		This would need handled through the BG scripting, since Ball Handling is done through OnUse ItemScript
		This does successfully eliminate the "Hand Pointer" and allow you to use 'GetSelected' functions properly.
]]
			
--[[	MW:		--
	Added random comments.
	Added LoS check.
	Cleaned up some code, yet I am not satisfied with it\s current state at all.
]]
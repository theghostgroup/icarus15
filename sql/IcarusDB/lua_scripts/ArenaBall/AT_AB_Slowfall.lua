local NPC_ID = 130003 -- SlowFall NPC ID
local Spell_Range = 1 -- Adjustable range check
     
function AreaTrigger_OnSpawn(event, creature)
    creature:SetVisible(false)
    creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Spell_Range)) then
            plr:CastSpell(plr, 130, true)
				print("Player in range, and trigged Slow Fall spell")
	end
end

RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)
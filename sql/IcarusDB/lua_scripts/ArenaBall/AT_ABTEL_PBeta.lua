local NPC_ID = 130005 -- Invisible Word Trigger 
local Teleport_Range = 1 -- Range in yards
     
function AreaTrigger_OnSpawn(event, creature)
    creature:SetVisible(false)
    creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Teleport_Range)) then
                plr:Teleport(870, -238.047, -5425.184, 187, 1.36) -- Platform Epsilon
        end
end
  
RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)

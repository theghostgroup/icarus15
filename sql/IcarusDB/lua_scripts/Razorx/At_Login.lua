function At_Login(event, player)
	if player:GetGuildName() == "Icarus-Staff" and not player:HasTitle(4125) then
		player:SetKnownTitle(4125)
	elseif player:HasTitle(4125) and not player:GetGuildName() == "Icarus-Staff" then
		player:UnsetKnownTitle(4125)
	end
end

RegisterPlayerEvent(3, At_Login)
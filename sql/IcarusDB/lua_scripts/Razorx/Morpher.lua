local NPC_ID = 22852
     
local function On_GossipMenu(event, player, unit)
    if (player:IsInCombat()) then
        player:SendNotification("You can not do that when in combat!")
    else
		player:GossipClearMenu()
		--player:GossipMenuAddItem(9, "TEST", 0, 9000, 0)
		player:GossipMenuAddItem(9, "Demorph", 0, 1, 0)
		player:GossipMenuAddItem(9, "Slime", 0, 2, 0)
        player:GossipMenuAddItem(9, "Succubus", 0, 3, 0)
		player:GossipMenuAddItem(9, "Dreadlord", 0, 4, 0)
		player:GossipMenuAddItem(9, "Blood Shade", 0, 5, 0)
		player:GossipMenuAddItem(9, "Dreaming Whelp", 0, 6, 0)
		player:GossipMenuAddItem(9, "FelGuard", 0, 7, 0)
		player:GossipMenuAddItem(9, "Death Knight (Male)", 0, 8, 0)
		player:GossipMenuAddItem(9, "Death Knight (Female)", 0, 9, 0)
		player:GossipMenuAddItem(9, "Skeleton", 0, 10, 0)
		player:GossipMenuAddItem(9, "Frost Dwarf", 0, 11, 0)
		player:GossipMenuAddItem(9, "Zen'tabra Cat", 0, 12, 0)
		player:GossipMenuAddItem(9, "Frog", 0, 13, 0)
		player:GossipMenuAddItem(9, "Ghoul", 0, 14, 0)
		player:GossipMenuAddItem(9, "Random Pirate", 0, 15, 0)
		player:GossipMenuAddItem(9, "Leper Gnome", 0, 19, 0)
		player:GossipMenuAddItem(0, "Page 2", 0, 1000, 0)
		player:GossipSendMenu(1, unit)
		return
	end
end
	
local function On_GossipSelect(event, player, unit, sender, intid, code)
if (intid == 9000) then
	local group = player:GetGroup()
		--player:Teleport(player:GetGroup(), 996, -1021.0156, -3206.9055, 21.5449, 6067, 0)
		group:Teleport(996, -1021.0156, -3206.9055, 21.5449, 3.174)
	end
if (intid == 1000) then
		player:GossipClearMenu()
		player:GossipMenuAddItem(9, "Ninja (Male)", 0, 17, 0)
		player:GossipMenuAddItem(9, "Ninja (Female)", 0, 18, 0)
		player:GossipMenuAddItem(9, "Red Ogre", 0, 20, 0)
		player:GossipMenuAddItem(9, "Boar", 0, 21, 0)
		player:GossipMenuAddItem(9, "Illidan", 0, 22, 0)
		player:GossipMenuAddItem(9, "The Lich King", 0, 23, 0)
		player:GossipMenuAddItem(9, "Yeti", 0, 24, 0)
		player:GossipMenuAddItem(9, "Taunka", 0, 25, 0)
		player:GossipMenuAddItem(9, "Alexstrasza", 0, 26, 0)
		player:GossipMenuAddItem(9, "Akama", 0, 27, 0)
		player:GossipMenuAddItem(9, "Dark Shade", 0, 28, 0)
		player:GossipMenuAddItem(9, "Mechagnome Pilot", 0, 29, 0)
		player:GossipMenuAddItem(9, "Goblin (Male)", 0, 30, 0)
		player:GossipMenuAddItem(9, "Goblin (Female)", 0, 31, 0)
		player:GossipMenuAddItem(9, "Sara", 0, 38, 0)
		player:GossipMenuAddItem(0, "Page 1", 0, 1, 0)
		player:GossipMenuAddItem(0, "Page 3", 0, 1001, 0)
		player:GossipSendMenu(1, unit)
		return
	end
if (intid == 1001) then
		player:GossipClearMenu()
		player:GossipMenuAddItem(9, "Worgen (Male)", 0, 32, 0)
		player:GossipMenuAddItem(9, "Worgen (Female)", 0, 33, 0)
		player:GossipMenuAddItem(9, "Pandaren (Male)", 0, 34, 0)
		player:GossipMenuAddItem(9, "Pandaren (Female)", 0, 35, 0)
		player:GossipMenuAddItem(9, "Polar Bear", 0, 36, 0)
		player:GossipMenuAddItem(9, "Amani War Bear", 0, 37, 0)
		player:GossipMenuAddItem(9, "Blood-Queen Lana'thel", 0, 39, 0)
		player:GossipMenuAddItem(9, "Algalon Test Creature", 0, 40, 0)
		player:GossipMenuAddItem(9, "Algalon The Observer", 0, 41, 0)
		player:GossipMenuAddItem(9, "Priestess Tyriona", 0, 42, 0)
		player:GossipMenuAddItem(9, "Tyrion", 0, 43, 0)
		player:GossipMenuAddItem(9, "Undead (Male)", 0, 44, 0)
		player:GossipMenuAddItem(9, "Undead (Female)", 0, 45, 0)
		player:GossipMenuAddItem(9, "Blue Frog", 0, 46, 0)
		player:GossipMenuAddItem(9, "Human Pirate (Male)", 0, 16, 0)
		player:GossipMenuAddItem(9, "Human Pirate (Female)", 0, 47, 0)
		player:GossipMenuAddItem(0, "Page 2", 0, 1000, 0)
		player:GossipSendMenu(1, unit)
		return
	end
	if (intid == 1) then
		player:DeMorph()
		player:SetScale(1)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 2) then
        player:SendAreaTriggerMessage("You are now a Slime!")
		player:SetDisplayId(12349)
		player:SetScale(1.2)
		player:CastSpell(player, 42152, true)
	elseif (intid == 3) then
        player:SendAreaTriggerMessage("You are now a Succubus!")
		player:SetDisplayId(10923)
		player:SetScale(0.8)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 4) then
        player:SendAreaTriggerMessage("You are now a Dreadlord!")
		player:SetDisplayId(27610)
		player:SetScale(0.5)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 5) then
        player:SendAreaTriggerMessage("You are now a Blood Shade!")
		player:SetDisplayId(22702)
		player:SetScale(0.4)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 6) then
        player:SendAreaTriggerMessage("You are now a Dreaming Whelp!")
		player:SetDisplayId(621)
		player:SetScale(0.7)
		player:CastSpell(player, 42152, true)
	elseif (intid == 7) then
        player:SendAreaTriggerMessage("You are now a FelGuard!")
		player:SetDisplayId(14255)
		player:SetScale(0.7)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 8) then
        player:SendAreaTriggerMessage("You are now a Death Knight (Male)!")
		player:SetDisplayId(26571)
		player:SetScale(0.7)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 9) then
        player:SendAreaTriggerMessage("You are now a Death Knight (Female)!")
		player:SetDisplayId(26550)
		player:SetScale(0.7)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 10) then
        player:SendAreaTriggerMessage("You are now a Skeleton!")
		player:SetDisplayId(9786)
		player:SetScale(1.3)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 11) then
        player:SendAreaTriggerMessage("You are now a Frost Dwarf!")
        player:SetDisplayId(26460)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 12) then
        player:SendAreaTriggerMessage("You are now a Zen'tabra Cat!")
        player:SetDisplayId(31765)
		player:CastSpell(player, 42152, true)
	elseif (intid == 13) then
        player:SendAreaTriggerMessage("You are now a Frog!")
        player:SetDisplayId(21950)
		player:CastSpell(player, 42152, true)
	elseif (intid == 14) then
        player:SendAreaTriggerMessage("You are now a Ghoul!")
        player:SetDisplayId(22496)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 15) then
        player:SendAreaTriggerMessage("You are now a Random Pirate!")
        player:CastSpell(player, 24708, true)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 16) then
        player:SendAreaTriggerMessage("You are now a Human Pirate (Male)!")
        player:SetDisplayId(4620)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 17) then
        player:SendAreaTriggerMessage("You are now a Ninja (Male)!")
        player:SetDisplayId(4617)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 18) then
        player:SendAreaTriggerMessage("You are now a Ninja (Female)!")
        player:SetDisplayId(4618)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 19) then
        player:SendAreaTriggerMessage("You are now a Leper Gnome!")
        player:SetDisplayId(6929)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 20) then
        player:SendAreaTriggerMessage("You are now a Red Ogre!")
        player:SetDisplayId(19752)
		player:SetScale(0.9)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 21) then
        player:SendAreaTriggerMessage("You are now a Boar!")
        player:SetDisplayId(4714)
		player:SetScale(0.7)
		player:CastSpell(player, 42152, true)
	elseif (intid == 22) then
        player:SendAreaTriggerMessage("You are now Illidan!")
		player:SetDisplayId(27571)
		player:SetScale(0.4)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 23) then
        player:SendAreaTriggerMessage("You are now The Lich King!")
		player:SetDisplayId(24191)
		player:SetScale(0.6)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 24) then
        player:SendAreaTriggerMessage("You are now a Yeti!")
		player:SetDisplayId(22486)
		player:SetScale(0.4)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 25) then
        player:SendAreaTriggerMessage("You are now a Taunka!")
		player:SetDisplayId(24970)
		player:SetScale(0.8)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 26) then
        player:SendAreaTriggerMessage("You are now Alexstrasza!")
		player:SetDisplayId(28227)
		player:SetScale(0.7)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 27) then
        player:SendAreaTriggerMessage("You are now Akama!")
		player:SetDisplayId(20681)
		player:SetScale(0.8)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 28) then
        player:SendAreaTriggerMessage("You are now a Dark Shade!")
		player:SetDisplayId(4629)
		player:SetScale(0.9)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 29) then
        player:SendAreaTriggerMessage("You are now a Mechagnome Pilot!")
        player:SetDisplayId(24115)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 30) then
        player:SendAreaTriggerMessage("You are now a Goblin (Male)!")
        player:SetDisplayId(20582)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 31) then
        player:SendAreaTriggerMessage("You are now a Goblin (Female)!")
        player:SetDisplayId(20583)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 32) then
        player:SendAreaTriggerMessage("You are now a Worgen (Male)!")
        player:SetDisplayId(26269)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 33) then
        player:SendAreaTriggerMessage("You are now a Worgen (Female)!")
        player:SetDisplayId(37914)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 34) then
        player:SendAreaTriggerMessage("You are now a Pandaren (Male)!")
        player:SetDisplayId(54000)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 35) then
        player:SendAreaTriggerMessage("You are now a Pandaren (Female)!")
        player:SetDisplayId(54001)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 36) then
        player:SendAreaTriggerMessage("You are now a Polar Bear!")
        player:SetDisplayId(865)
		player:SetScale(0.9)
		player:CastSpell(player, 42152, true)
	elseif (intid == 37) then
        player:SendAreaTriggerMessage("You are now a Amani War Bear!")
		player:SetDisplayId(22464)
		player:SetScale(0.8)
		player:CastSpell(player, 42152, true)
	elseif (intid == 38) then
        player:SendAreaTriggerMessage("You are now Sara!")
		player:SetDisplayId(29182)
		player:SetScale(0.4)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 39) then
        player:SendAreaTriggerMessage("You are now Blood-Queen Lana'thel!")
		player:SetDisplayId(31093)
		player:SetScale(0.35)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 40) then
        player:SendAreaTriggerMessage("You are now Algalon Test Creature!")
		player:SetDisplayId(26239)
		player:SetScale(0.7)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 41) then
        player:SendAreaTriggerMessage("You are now Algalon The Observer!")
		player:SetDisplayId(28641)
		player:SetScale(0.18)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 42) then
        player:SendAreaTriggerMessage("You are now Priestess Tyriona!")
		player:SetDisplayId(6703)
		player:SetScale(1.1)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 43) then
        player:SendAreaTriggerMessage("You are now Tyrion!")
		player:SetDisplayId(6632)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 44) then
        player:SendAreaTriggerMessage("You are now a Undead (Male)!")
		player:SetDisplayId(1589)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 45) then
        player:SendAreaTriggerMessage("You are now a Undead (Female)!")
		player:SetDisplayId(4175)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 46) then
        player:SendAreaTriggerMessage("You are now a Blue Frog!")
		player:SetDisplayId(23311)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
	elseif (intid == 47) then
        player:SendAreaTriggerMessage("You are now a Human Pirate (Female)!")
        player:SetDisplayId(4619)
		local aura = player:GetAura(42152)
        if(aura) then
            aura:Remove()
        end
    end
    On_GossipMenu(event, player, unit)
end
RegisterCreatureGossipEvent(NPC_ID, 1, On_GossipMenu)
RegisterCreatureGossipEvent(NPC_ID, 2, On_GossipSelect)
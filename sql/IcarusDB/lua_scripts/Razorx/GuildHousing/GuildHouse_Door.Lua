local function GetDoorOwner(playername)
	local plrOwnsDoor = WorldDBQuery("SELECT DoorGuid FROM guild_houses WHERE PlayerName='" .. playername .. "'")
    if(plrOwnsDoor) then
			plrOwnsDoor = plrOwnsDoor:GetUInt32(0)
    return plrOwnsDoor
	end
	return false
end

local function GetHouseOwner(guid)
	local HouseOwner = WorldDBQuery("SELECT Guild FROM guild_houses WHERE DoorGuid='" .. guid .. "'")
    if(HouseOwner) then
			HouseOwner = HouseOwner:GetString(0)
    return "Owned"
	end
	if(HouseOwner) == nil then
	return "Not Owned"
	end
end

local function GetHouseOwner2(guid)
	local HouseOwner = WorldDBQuery("SELECT Guild FROM guild_houses WHERE DoorGuid='" .. guid .. "'")
    if(HouseOwner) then
			HouseOwner = HouseOwner:GetString(0)
    return HouseOwner
	end
end

local function GetCoordsMap(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsInstance(playername)
	local Coords = WorldDBQuery("SELECT Instance_Id FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsX(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsY(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsZ(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsO(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_vendor1_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end

local function GetCoordsMap_1(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsInstance_1(playername)
	local Coords = WorldDBQuery("SELECT Instance_Id FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsX_1(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsY_1(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsZ_1(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoordsO_1(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_vendor2_locs` WHERE PlayerName='" .. playername .. "'")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end


-- Door GUID's
local Door_01 = 216171
local Door_02 = 216177
local Door_03 = 216194
local Door_04 = 216201
local Door_05 = 216204
local Door_06 = 216208
local Door_07 = 216209
local Door_08 = 216210
local Door_09 = 216211
local Door_10 = 216212
local Door_11 = 217140
local Door_12 = 217142
local Door_13 = 217144
local Door_14 = 217146
local Door_15 = 217148
local Door_16 = 217138
local Door_17 = 217136
local Door_18 = 217134
local Door_19 = 217132
local Door_20 = 217130

GH_Doors = {}

function GH_Doors.customObj_GossipMenu(event, player, object)
	local clickedDoor = object:GetGUIDLow()
	local PlrName = player:GetName()
	local PlrGUID = player:GetGUID()
	if clickedDoor == Door_01 or clickedDoor == Door_02 or clickedDoor == Door_03 or clickedDoor == Door_04 or clickedDoor == Door_05 or clickedDoor == Door_06 or clickedDoor == Door_07 or clickedDoor == Door_08 or clickedDoor == Door_09 or clickedDoor == Door_10 or clickedDoor == Door_11 or clickedDoor == Door_12 or clickedDoor == Door_13 or clickedDoor == Door_14 or clickedDoor == Door_15 or clickedDoor == Door_16 or clickedDoor == Door_17 or clickedDoor == Door_18 or clickedDoor == Door_19 or clickedDoor == Door_20 then
		if clickedDoor == Door_01 then
			player:GossipMenuAddItem(0, "Guild House Number: 1", 0, 1000)
			elseif clickedDoor == Door_02 then
			player:GossipMenuAddItem(0, "Guild House Number: 2", 0, 1000)
			elseif clickedDoor == Door_03 then
			player:GossipMenuAddItem(0, "Guild House Number: 3", 0, 1000)
			elseif clickedDoor == Door_04 then
			player:GossipMenuAddItem(0, "Guild House Number: 4", 0, 1000)
			elseif clickedDoor == Door_05 then
			player:GossipMenuAddItem(0, "Guild House Number: 5", 0, 1000)
			elseif clickedDoor == Door_06 then
			player:GossipMenuAddItem(0, "Guild House Number: 6", 0, 1000)
			elseif clickedDoor == Door_07 then
			player:GossipMenuAddItem(0, "Guild House Number: 7", 0, 1000)
			elseif clickedDoor == Door_08 then
			player:GossipMenuAddItem(0, "Guild House Number: 8", 0, 1000)
			elseif clickedDoor == Door_09 then
			player:GossipMenuAddItem(0, "Guild House Number: 9", 0, 1000)
			elseif clickedDoor == Door_10 then
			player:GossipMenuAddItem(0, "Guild House Number: 10", 0, 1000)
			elseif clickedDoor == Door_11 then
			player:GossipMenuAddItem(0, "Guild House Number: 11", 0, 1000)
			elseif clickedDoor == Door_12 then
			player:GossipMenuAddItem(0, "Guild House Number: 12", 0, 1000)
			elseif clickedDoor == Door_13 then
			player:GossipMenuAddItem(0, "Guild House Number: 13", 0, 1000)
			elseif clickedDoor == Door_14 then
			player:GossipMenuAddItem(0, "Guild House Number: 14", 0, 1000)
			elseif clickedDoor == Door_15 then
			player:GossipMenuAddItem(0, "Guild House Number: 15", 0, 1000)
			elseif clickedDoor == Door_16 then
			player:GossipMenuAddItem(0, "Guild House Number: 16", 0, 1000)
			elseif clickedDoor == Door_17 then
			player:GossipMenuAddItem(0, "Guild House Number: 17", 0, 1000)
			elseif clickedDoor == Door_18 then
			player:GossipMenuAddItem(0, "Guild House Number: 18", 0, 1000)
			elseif clickedDoor == Door_19 then
			player:GossipMenuAddItem(0, "Guild House Number: 19", 0, 1000)
			elseif clickedDoor == Door_20 then
			player:GossipMenuAddItem(0, "Guild House Number: 20", 0, 1000)
		end
		if GetHouseOwner2(clickedDoor) then
			player:GossipMenuAddItem(0, "Status: "..GetHouseOwner(clickedDoor).." by "..GetHouseOwner2(clickedDoor).."", 0, 1000)
		else
			player:GossipMenuAddItem(0, "Status: "..GetHouseOwner(clickedDoor).."", 0, 1000)
		end
		player:GossipSendMenu(1, object)
	end
	if clickedDoor == GetDoorOwner(PlrName) then
		player:GossipMenuAddItem(2, "Teleport To Vendor", 0, 1)
		player:GossipMenuAddItem(3, "Open Door", 0, 4)
		player:GossipMenuAddItem(4, "Options", 0, 3)
		player:GossipSendMenu(1, object)
	end
end

--RegisterGameObjectGossipEvent(533680, 1, GH_Doors.customObj_GossipMenu)
--RegisterGameObjectGossipEvent(4002170, 1, GH_Doors.customObj_GossipMenu)
--RegisterGameObjectGossipEvent(4002171, 1, GH_Doors.customObj_GossipMenu)
RegisterGameObjectGossipEvent(4002172, 1, GH_Doors.customObj_GossipMenu)
--RegisterGameObjectGossipEvent(4002173, 1, GH_Doors.customObj_GossipMenu)

function GH_Doors.customObj_OnSelect(event, player, object, sender, intid, code)
	local clickedDoor = object:GetGUIDLow()
	local PlrName = player:GetName()
	local vendor = WorldDBQuery("SELECT Vendor FROM guild_houses WHERE PlayerName='" .. player:GetName() .. "'"):GetUInt32(0)
	local vendor2 = WorldDBQuery("SELECT Vendor2 FROM guild_houses WHERE PlayerName='" .. player:GetName() .. "'"):GetUInt32(0)
	if (intid == 1000) then
		player:GossipComplete()
	end	
	if (intid == 3) then
		player:GossipClearMenu()
		player:GossipMenuAddItem(10, "Buy/Sell General Goods Vendor, 20 Gold", 0, 5)
		player:GossipMenuAddItem(10, "Buy/Sell Guild House Vendor, 20 Gold", 0, 7)
		player:GossipMenuAddItem(10, "Buy/Sell Homeowner Title, 10 Gold", 0, 8)
		--player:GossipMenuAddItem(2, 'Summon a Friend', 0, 9, true, "Click Accept then enter the name of the player you want to summon.")
		player:GossipMenuAddItem(2, "Set As Home", 0, 10, 0)
		--player:GossipMenuAddItem(0, "Close Menu", 0, 1000)
	    player:GossipSendMenu(1, object)
		return
	end
	if(intid == 10) then
		player:CastSpell(player, 52096, true)
		player:CastSpell(player, 26, true)
		player:GossipComplete()
	end
	if (intid == 9) then
		for k,v in pairs (GetPlayersInWorld()) do
			if v:GetName() == PlrName then
				player:GossipComplete()
			elseif v:GetName():lower() == code:lower() then
				player:SummonPlayer(v, player:GetMapId(), player:GetX()+0.45, player:GetY()-0.45, player:GetZ(), player:GetZoneId(), 0)
			end
		player:GossipComplete()
		end
	end
	if (intid == 5) then
		if vendor ~= 0 then
			local vendorCrea = WorldDBQuery("SELECT Vendor FROM guild_houses WHERE PlayerName ='".. player:GetName() .."'"):GetUInt32(0)
			for k,v in pairs (player:GetFriendlyUnitsInRange()) do
				if v and v:GetGUIDLow() == vendorCrea then
					v:DespawnOrUnsummon(0)
					WorldDBQuery("UPDATE guild_houses SET Vendor = NULL WHERE PlayerName = '".. player:GetName() .."'")
					WorldDBQuery("DELETE FROM creature WHERE guid = '".. vendorCrea .."'")
				end
			end
			player:ModifyMoney(200000)
			elseif clickedDoor == Door_01 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 1
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_02 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 2
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_03 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 3
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_04 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 4
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_05 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 5
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_06 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 6
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_07 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 7
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_08 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 8
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)			
			elseif clickedDoor == Door_09 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 9
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_10 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 10
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_11 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 11
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_12 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 12
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_13 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 13
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_14 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 14
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_15 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 15
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_16 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 16
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_17 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 17
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_18 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 18
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)			
			elseif clickedDoor == Door_19 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 19
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_20 then
				local vendorCrea = PerformIngameSpawn(1, 37904, GetCoordsMap(PlrName), GetCoordsInstance(PlrName), GetCoordsX(PlrName), GetCoordsY(PlrName), GetCoordsZ(PlrName)+3, GetCoordsO(PlrName), 1) -- House 20
				WorldDBQuery("UPDATE guild_houses SET Vendor = '".. vendorCrea:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
		end
		player:GossipComplete()
	end
	if (intid == 1) then		
		player:Teleport(169, 3388.73,  -1234.69, 94.00, 0)
	end
	if (intid == 4) then
		object:UseDoorOrButton()
		player:GossipComplete()
	end
	if (intid == 7) then
		if vendor2 ~= 0 then
			local vendorCrea2 = WorldDBQuery("SELECT Vendor2 FROM guild_houses WHERE PlayerName ='".. player:GetName() .."'"):GetUInt32(0)
			for k,v in pairs (player:GetFriendlyUnitsInRange()) do
				if v and v:GetGUIDLow() == vendorCrea2 then
					v:DespawnOrUnsummon(0)
					WorldDBQuery("UPDATE guild_houses SET Vendor2 = NULL WHERE PlayerName = '".. player:GetName() .."'")
					WorldDBQuery("DELETE FROM creature WHERE guid = '".. vendorCrea2 .."'")
				end
			end
			player:ModifyMoney(200000)
			elseif clickedDoor == Door_01 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 1
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_02 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 2
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_03 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 3
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_04 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 4
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_05 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 5
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_06 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 6
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_07 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 7
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_08 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 8
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)			
			elseif clickedDoor == Door_09 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 9
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_10 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 10
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_11 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 11
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_12 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 12
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_13 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 13
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_14 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 14
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_15 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 15
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_16 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 16
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_17 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 17
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_18 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 18
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)			
			elseif clickedDoor == Door_19 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 19
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
			elseif clickedDoor == Door_20 then
				local vendorCrea2 = PerformIngameSpawn(1, 5000010, GetCoordsMap_1(PlrName), GetCoordsInstance_1(PlrName), GetCoordsX_1(PlrName), GetCoordsY_1(PlrName), GetCoordsZ_1(PlrName)+3, GetCoordsO_1(PlrName), 1) -- House 20
				WorldDBQuery("UPDATE guild_houses SET Vendor2 = '".. vendorCrea2:GetGUIDLow() .."' WHERE PlayerName = '".. player:GetName() .."'")
				player:ModifyMoney(-200000)
		end
		player:GossipComplete()
	end
	if (intid == 8) then
		if player:HasTitle(3202) then
			player:UnsetKnownTitle(3202)  -- Remove Homeowner Title
			player:ModifyMoney(100000)
			player:GossipComplete()
		elseif player:HasTitle(3202) == false then
			player:SetKnownTitle(3202)  -- Give Homeowner Title
			player:ModifyMoney(-100000)
			player:GossipComplete()
		end
	end
end

--RegisterGameObjectGossipEvent(533680, 2, GH_Doors.customObj_OnSelect)
--RegisterGameObjectGossipEvent(4002170, 2, GH_Doors.customObj_OnSelect)
--RegisterGameObjectGossipEvent(4002171, 2, GH_Doors.customObj_OnSelect)
RegisterGameObjectGossipEvent(4002172, 2, GH_Doors.customObj_OnSelect)
--RegisterGameObjectGossipEvent(4002173, 2, GH_Doors.customObj_OnSelect)
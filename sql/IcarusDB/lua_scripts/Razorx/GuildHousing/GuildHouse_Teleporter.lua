-- #############################
-- ##    Shared by RazorX14   ##
-- ##       For BotCore       ##
-- #############################


--> GUILD HOUSE TELEPORTER CODE


local TELEPORTER_ID = 80005


--> DO NOT ALTER ANYTHING BELOW THIS POINT!!!

local function GetGuildFromGuildHouses1(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 1")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses2(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 2")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses3(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 3")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses4(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 4")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses5(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 5")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses6(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 6")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses7(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 7")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses8(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 8")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses9(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 9")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses10(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 10")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses11(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 11")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses12(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 12")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses13(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 13")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses14(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 14")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses15(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 15")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses16(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 16")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses17(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 17")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses18(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 18")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses19(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 19")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses20(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 20")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses21(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 21")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end

local function GetGuildIdFromGuildHouses(guildid)
	local GuildId = WorldDBQuery("SELECT `GuildId` FROM `guild_houses` WHERE `GuildId` LIKE " .. guildid .. "")
    if(GuildId) then
			gguid = GuildId:GetUInt32(0)
    return gguid
	end
	return false
end

local function GetGUID(playername)
	local GUID = CharDBQuery("SELECT guid from characters where name='" .. playername .. "'")
	local id = 0
	if(GUID) then
        repeat
            id = GUID:GetUInt32(0)
        until not GUID:NextRow()
	end
    return id
end

local function GetGuildHouseFromGuildHouses(guildid)
	local GuildHouse = WorldDBQuery("SELECT `GuildHouse` FROM `guild_houses` WHERE `GuildId` LIKE " .. guildid .. "")
    if(GuildHouse) then
			guildh = GuildHouse:GetUInt32(0)
    return guildh
	end
	return false
end

local function GetCoords1(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords2(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords3(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords4(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords5(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 1")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords6(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords7(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords8(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords9(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords10(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 2")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords11(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords12(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords13(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords14(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords15(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 3")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords16(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords17(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords18(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords19(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords20(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 4")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords21(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords22(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords23(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords24(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords25(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 5")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords26(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords27(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords28(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords29(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords30(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 6")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords31(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords32(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords33(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords34(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords35(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 7")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords36(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords37(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords38(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords39(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords40(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 8")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords41(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords42(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords43(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords44(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords45(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 9")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords46(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords47(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords48(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords49(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords50(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 10")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords51(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords52(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords53(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords54(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords55(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 11")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords56(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords57(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords58(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords59(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords60(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 12")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords61(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords62(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords63(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords64(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords65(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 13")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords66(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords67(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords68(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords69(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords70(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 14")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords71(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords72(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords73(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords74(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords75(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 15")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords76(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords77(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords78(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords79(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords80(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 16")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords81(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords82(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords83(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords84(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords85(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 17")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords86(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords87(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords88(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords89(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords90(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 18")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords91(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords92(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords93(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords94(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords95(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 19")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords96(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords97(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords98(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords99(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords100(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 20")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords101(playername)
	local Coords = WorldDBQuery("SELECT Map FROM `guild_houses_locs` WHERE `GuildHouse` = 21")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords102(playername)
	local Coords = WorldDBQuery("SELECT X FROM `guild_houses_locs` WHERE `GuildHouse` = 21")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords103(playername)
	local Coords = WorldDBQuery("SELECT Y FROM `guild_houses_locs` WHERE `GuildHouse` = 21")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords104(playername)
	local Coords = WorldDBQuery("SELECT Z FROM `guild_houses_locs` WHERE `GuildHouse` = 21")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end
local function GetCoords105(playername)
	local Coords = WorldDBQuery("SELECT O FROM `guild_houses_locs` WHERE `GuildHouse` = 21")
    if(Coords) then
			coords = Coords:GetString(0)
    return coords
	end
	return false
end

local function GossipMenu(event, player, unit)
    local PlrName = player:GetName()
	if (player:GetGuildName() == NULL) then
		player:SendNotification("You Dont Have A Guild "..PlrName.."")
		player:GossipComplete()
	   return
	else
        player:GossipMenuAddItem(2, "[GM] "..GetGuildFromGuildHouses1(player:GetName()).." Guild House", 0, 2, 0)
		player:GossipMenuAddItem(2, "[01] "..GetGuildFromGuildHouses2(player:GetName()).." Guild House", 0, 3, 0)
		player:GossipMenuAddItem(2, "[02] "..GetGuildFromGuildHouses3(player:GetName()).." Guild House", 0, 4, 0)
		player:GossipMenuAddItem(2, "[03] "..GetGuildFromGuildHouses4(player:GetName()).." Guild House", 0, 5, 0)
		player:GossipMenuAddItem(2, "[04] "..GetGuildFromGuildHouses5(player:GetName()).." Guild House", 0, 6, 0)
		player:GossipMenuAddItem(2, "[05] "..GetGuildFromGuildHouses6(player:GetName()).." Guild House", 0, 7, 0)
		player:GossipMenuAddItem(2, "[06] "..GetGuildFromGuildHouses7(player:GetName()).." Guild House", 0, 8, 0)
		player:GossipMenuAddItem(2, "[07] "..GetGuildFromGuildHouses8(player:GetName()).." Guild House", 0, 9, 0)
		player:GossipMenuAddItem(2, "[08] "..GetGuildFromGuildHouses9(player:GetName()).." Guild House", 0, 10, 0)
		player:GossipMenuAddItem(2, "[09] "..GetGuildFromGuildHouses10(player:GetName()).." Guild House", 0, 11, 0)
		player:GossipMenuAddItem(2, "[10] "..GetGuildFromGuildHouses11(player:GetName()).." Guild House", 0, 12, 0)
		player:GossipMenuAddItem(2, "[11] "..GetGuildFromGuildHouses12(player:GetName()).." Guild House", 0, 13, 0)
		player:GossipMenuAddItem(2, "[12] "..GetGuildFromGuildHouses13(player:GetName()).." Guild House", 0, 14, 0)
		player:GossipMenuAddItem(2, "[13] "..GetGuildFromGuildHouses14(player:GetName()).." Guild House", 0, 15, 0)
		player:GossipMenuAddItem(2, "[14] "..GetGuildFromGuildHouses15(player:GetName()).." Guild House", 0, 16, 0)
		player:GossipMenuAddItem(2, "[15] "..GetGuildFromGuildHouses16(player:GetName()).." Guild House", 0, 17, 0)
		player:GossipMenuAddItem(2, "[16] "..GetGuildFromGuildHouses17(player:GetName()).." Guild House", 0, 18, 0)
		player:GossipMenuAddItem(2, "[17] "..GetGuildFromGuildHouses18(player:GetName()).." Guild House", 0, 19, 0)
		player:GossipMenuAddItem(2, "[18] "..GetGuildFromGuildHouses19(player:GetName()).." Guild House", 0, 20, 0)
		player:GossipMenuAddItem(2, "[19] "..GetGuildFromGuildHouses20(player:GetName()).." Guild House", 0, 21, 0)
		player:GossipMenuAddItem(2, "[20] "..GetGuildFromGuildHouses21(player:GetName()).." Guild House", 0, 22, 0)
		player:GossipSendMenu(1, unit)
    end
end

function DelayMessage(player)
	local PlrName = player:GetName()
	player:SendNotification("Welcome To The "..GetGuildFromGuildHouses1(player:GetName()).." Guild House, "..PlrName.."")
end

local function GossipSelect(event, player, unit, sender, intid, code)
	local PlrName = player:GetName()
	if (intid == 2) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 1 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords1(player:GetName()),GetCoords2(player:GetName()),GetCoords3(player:GetName()),GetCoords4(player:GetName())+2,GetCoords5(player:GetName()))
			player:RegisterEvent(function() DelayMessage(player) end, 3500, 1)
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 3) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 2 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords6(player:GetName()),GetCoords7(player:GetName()),GetCoords8(player:GetName()),GetCoords9(player:GetName())+2,GetCoords10(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses2(player:GetName()).." Guild House, "..PlrName.."")
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 4) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 3 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords11(player:GetName()),GetCoords12(player:GetName()),GetCoords13(player:GetName()),GetCoords14(player:GetName())+2,GetCoords15(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses3(player:GetName()).." Guild House, "..PlrName.."")
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 5) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 4 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords16(player:GetName()),GetCoords17(player:GetName()),GetCoords18(player:GetName()),GetCoords19(player:GetName())+2,GetCoords20(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses4(player:GetName()).." Guild House, "..PlrName.."")
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 6) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 5 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords21(player:GetName()),GetCoords22(player:GetName()),GetCoords23(player:GetName()),GetCoords24(player:GetName())+2,GetCoords25(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses5(player:GetName()).." Guild House, "..PlrName.."")
			player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 7) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 6 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords26(player:GetName()),GetCoords27(player:GetName()),GetCoords28(player:GetName()),GetCoords29(player:GetName())+2,GetCoords30(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses6(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 8) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 7 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords31(player:GetName()),GetCoords32(player:GetName()),GetCoords33(player:GetName()),GetCoords34(player:GetName())+2,GetCoords35(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses7(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 9) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 8 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords36(player:GetName()),GetCoords37(player:GetName()),GetCoords38(player:GetName()),GetCoords39(player:GetName())+2,GetCoords40(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses8(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 10) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 9 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords41(player:GetName()),GetCoords42(player:GetName()),GetCoords43(player:GetName()),GetCoords44(player:GetName())+2,GetCoords45(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses9(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 11) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 10 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords46(player:GetName()),GetCoords47(player:GetName()),GetCoords48(player:GetName()),GetCoords49(player:GetName())+2,GetCoords50(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses10(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 12) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 11 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords51(player:GetName()),GetCoords52(player:GetName()),GetCoords53(player:GetName()),GetCoords54(player:GetName())+2,GetCoords55(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses11(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 13) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 12 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords56(player:GetName()),GetCoords57(player:GetName()),GetCoords58(player:GetName()),GetCoords59(player:GetName())+2,GetCoords60(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses12(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 14) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 13 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords61(player:GetName()),GetCoords62(player:GetName()),GetCoords63(player:GetName()),GetCoords64(player:GetName())+2,GetCoords65(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses13(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 15) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 14 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords66(player:GetName()),GetCoords67(player:GetName()),GetCoords68(player:GetName()),GetCoords69(player:GetName())+2,GetCoords70(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses14(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 16) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 15 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords71(player:GetName()),GetCoords72(player:GetName()),GetCoords73(player:GetName()),GetCoords74(player:GetName())+2,GetCoords75(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses15(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 17) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 16 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords76(player:GetName()),GetCoords77(player:GetName()),GetCoords78(player:GetName()),GetCoords79(player:GetName())+2,GetCoords80(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses16(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 18) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 17 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords81(player:GetName()),GetCoords82(player:GetName()),GetCoords83(player:GetName()),GetCoords84(player:GetName())+2,GetCoords85(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses17(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 19) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 18 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords86(player:GetName()),GetCoords87(player:GetName()),GetCoords88(player:GetName()),GetCoords89(player:GetName())+2,GetCoords90(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses18(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 20) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 19 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords91(player:GetName()),GetCoords92(player:GetName()),GetCoords93(player:GetName()),GetCoords94(player:GetName())+2,GetCoords95(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses19(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 21) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 20 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords96(player:GetName()),GetCoords97(player:GetName()),GetCoords98(player:GetName()),GetCoords99(player:GetName())+2,GetCoords100(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses20(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
	if (intid == 22) then
		if GetGuildIdFromGuildHouses(player:GetGuildId()) and GetGuildHouseFromGuildHouses(player:GetGuildId()) == 21 or player:GetName() == "Razorx" then
			player:Teleport(GetCoords101(player:GetName()),GetCoords102(player:GetName()),GetCoords103(player:GetName()),GetCoords104(player:GetName())+2,GetCoords105(player:GetName()))
			player:SendNotification("Welcome To "..GetGuildFromGuildHouses21(player:GetName()).." Guild House, "..PlrName.."")
		player:GossipComplete()
			else
				player:SendNotification("That Is Not Your Guild House!")
				player:GossipComplete()
			return
		end
	end
end

RegisterCreatureGossipEvent(TELEPORTER_ID, 1, GossipMenu)
RegisterCreatureGossipEvent(TELEPORTER_ID, 2, GossipSelect)


--< GUILD HOUSE TELEPORTER CODE